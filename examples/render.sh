#!/bin/bash

for example in basic sms discordant; do
  echo "==== Rendering example $example... ===="
  node ../fictional-messenger-cli $example.txt $example.json --include-css-with-html --output rendered/$example.html
done