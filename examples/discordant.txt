ponder: Firstly, Mr. Pessimal wants to know what we do here.
ridcully: Do? We are the premier college of magic!
ponder: But do we teach?
dean: Only if no alternative presents itself.
We show ‘em where the library is, give ‘em a few little chats, and graduate the survivors. If they run into any problems, my door is always metaphorically open.
ponder: Metaphorically, sir?
dean: Yes. But technically, of course, it’s locked.
lecturer: Explain to him that we don’t do things, Stibbons. We are academics. 
// from Terry Pratchett, A Blink of the Screen: Collected Shorter Fiction 