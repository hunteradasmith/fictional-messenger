crumley: You can't give her that! It's not safe!
hogfather: IT'S A SWORD. THEY'RE NOT MEANT TO BE SAFE.
crumley: She's a child!
hogfather: IT'S EDUCATIONAL.
crumley: What if she cuts herself?
hogfather: THAT WILL BE AN IMPORTANT LESSON.
// from Terry Pratchett, Hogfather