# fictional-messenger-app

fictional-messenger-app is a React application that renders text conversations with fancy HTML and CSS to imitate real-world message platforms.

* [Open the app](https://fictional.tools/messenger/?ref=gl)
* [Read the docs](https://fictional.tools/messenger/docs/?ref=gl)

## Development info

#### Setting up the project
First, set up and build the renderer module in `../fictional-messenger-renderer`. Then, just run `npm install` to install the needed dependencies.

The following NPM commands perform various development actions:

```bash
npm run start # start a Parcel dev server
npm run check # checks TypeScript
npm run lint # lints and prettifies
npm run lint:fix # lints in auto-fix mode
npm run build # builds the app and outputs files to ./dist/app/
npm run prerender # prerenders the app to static HTML
```

#### Prerendering

For SEO and improved page load times, this app uses a slightly hacky and overly simple method of prerendering to static HTML.

The `src/prerender.ts` file is a special entrypoint in the application that Parcel builds to `dist/prerender/`. When this file is run by Node, it uses ReactDOM to render the application's initial state to a string. The string is then inserted into the `#app-container` element in the `dist/app/index.html` file.

When the app runs in-browser, `src/index.ts` will check if `#app-container` already has content. If so, it'll hydrate it instead of rendering fresh.

This method of prerendering is a bit of a kludge, but I wasn't able to find any existing Parcel plugins to prerender to static files. (The app is hosted on GitLab Pages, so there's no actual server to do server-side rendering.)

#### Deployment

Deployment is handled via GitLab CI/CD. When the `main` branch is updated, it'll automatically build, prerender, and deploy the app.

Merge requests will be automatically tested by the CI pipeline as well.

#### Favicon generation

Favicons generated using [Real Favicon Generator](https://realfavicongenerator.net/). (Note: this only appears to work correctly on Chromium-based browsers.)

#### Analytics

Fictional Messenger uses [Plausible](https://plausible.io) to collect fully anonymous usage stats.

The following `?ref=` URL params are used to track which links lead users to FM:

* `gl`: FM's GitLab repo
* `docs`: FM's documentation
* `us`: user share link from the render results page
