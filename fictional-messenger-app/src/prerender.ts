/**
 * Overly-simple prerendering of the app's initial state. This allows
 * faster load times and makes the intro page readable to search crawlers.
 */

import { renderToString } from 'react-dom/server'
import { AppContainer } from './pages/app-container.tsx'
import { parse } from 'node-html-parser'
import fs from 'node:fs'

const indexPath = 'dist/app/index.html'

const indexFile = fs.readFileSync(indexPath).toString()
const page = parse(indexFile)

const prerendered = renderToString(AppContainer())
const container = page.querySelector('#app-container')

if (container === null) {
  throw new Error('Could not find app container element')
}

container.innerHTML = prerendered

fs.writeFileSync(indexPath, page.toString())
