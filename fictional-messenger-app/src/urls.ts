export const fmUrls = {
  fm: 'https://fictional.tools/messenger/',
  docs: 'https://fictional.tools/messenger/docs/?ref=fm',
  gitlab: 'https://gitlab.com/fictional.tools/fictional-messenger',
  discord: 'https://discord.gg/Q2bgvrjkCn',
}
