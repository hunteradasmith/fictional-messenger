/**
 * Global Plausible analytics function, defined in Plausible's external script.
 * The script may be blocked by certain browser, or be otherwise unavailable.
 */
declare function plausible(
  eventName: string,
  props?: { props?: Record<string, string> },
): void
