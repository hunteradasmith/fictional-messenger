import React, { JSX } from 'react'
import { CopyButton } from './copy-button.tsx'

export function CodeBlock({ value }: { value: string }): JSX.Element {
  return (
    <pre className="code-block d-grid gap-2 border border-secondary bg-light">
      <CopyButton text={value} />
      <code className="p-2">{value}</code>
    </pre>
  )
}
