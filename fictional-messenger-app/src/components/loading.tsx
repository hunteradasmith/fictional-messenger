import React, { JSX } from 'react'
import { Loader } from 'react-feather'

export function Loading(): JSX.Element {
  return (
    <h1>
      <Loader />
      Loading...
    </h1>
  )
}
