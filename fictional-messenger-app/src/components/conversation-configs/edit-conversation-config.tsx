import { type PrimitiveAtom, useAtom } from 'jotai'
import React, { JSX } from 'react'
import { styles, type ConversationConfig } from 'fictional-messenger-renderer'
import { useAtomProp } from '../../hooks/use-atom-prop.ts'
import { SettingsForm } from './settings-form.tsx'
import { EditUsers } from './edit-users.tsx'
import { StyleSelector } from './style-selector.tsx'

interface EditConversationConfigProps {
  conversationConfigName: string
  conversationConfigAtom: PrimitiveAtom<ConversationConfig>
}

export function EditConversationConfig({
  conversationConfigName,
  conversationConfigAtom,
}: EditConversationConfigProps): JSX.Element {
  const [style] = useAtom(useAtomProp(conversationConfigAtom, 'style'))
  const styleInfo = styles.infoByName[style]

  return (
    <>
      <div className="border-bottom border-secondary mt-3">
        <h3>{conversationConfigName}</h3>
      </div>

      <div className="ms-1">
        <div className="my-4">
          <h4>Style</h4>
          <StyleSelector
            styleAtom={useAtomProp(conversationConfigAtom, 'style')}
          />
        </div>

        <div className="my-2">
          <h4>Settings</h4>
          <SettingsForm
            settingsAtom={useAtomProp(conversationConfigAtom, 'settingValues')}
            settingsInfo={styleInfo.settings}
            elementIdPrefix={`${conversationConfigName}-settings`}
          />
        </div>

        <div className="my-2">
          <h4>Users</h4>
          <EditUsers
            usersAtom={useAtomProp(conversationConfigAtom, 'users')}
            settingsInfo={styleInfo.userSettings}
            elementIdPrefix={`${conversationConfigName}-userSettings`}
          />
        </div>
      </div>
    </>
  )
}
