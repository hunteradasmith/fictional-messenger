import { type SettingInfo } from 'fictional-messenger-renderer'
import { type PrimitiveAtom, useAtom } from 'jotai'
import React, { JSX } from 'react'
import { Button, Col, Form, InputGroup, Row } from 'react-bootstrap'
import { useAtomProp } from '../../hooks/use-atom-prop.ts'
import { SettingType } from 'fictional-messenger-renderer'
import { SettingColorPicker } from './setting-color-picker.tsx'

export interface SettingInputElementProps {
  id: string
  value: any
  onChange: (value: any) => void
}

const inputElementsByType: Record<
  SettingType,
  (props: SettingInputElementProps) => JSX.Element
> = {
  [SettingType.String]: ({ id, value, onChange }) => (
    <Form.Control
      type="text"
      id={id}
      name={id}
      value={value}
      onChange={(e) => {
        onChange(e.target.value)
      }}
    />
  ),

  [SettingType.Number]: ({ id, value, onChange }) => (
    <Form.Control
      type="number"
      id={id}
      name={id}
      value={value}
      onChange={(e) => {
        onChange(parseFloat(e.target.value))
      }}
    />
  ),

  [SettingType.Boolean]: ({ id, value, onChange }) => (
    <InputGroup id={id}>
      <Button
        variant="outline-secondary"
        active={value !== true}
        onClick={() => {
          onChange(false)
        }}
      >
        No
      </Button>
      <Button
        variant="outline-primary"
        active={value === true}
        onClick={() => {
          onChange(true)
        }}
      >
        Yes
      </Button>
    </InputGroup>
  ),

  [SettingType.HslColor]: SettingColorPicker,
}

interface SettingInputProps {
  settingsAtom: PrimitiveAtom<Record<string, any>>
  settingInfo: SettingInfo
  elementIdPrefix: string
}

export function SettingInput({
  settingsAtom,
  settingInfo,
  elementIdPrefix,
}: SettingInputProps): JSX.Element {
  const settingAtom = useAtomProp(settingsAtom, settingInfo.name)
  const [value, setValue] = useAtom(settingAtom)
  const elementId = `${elementIdPrefix}-${settingInfo.name}`

  const InputElement = inputElementsByType[settingInfo.type]
  // TODO validation

  return (
    <Form.Group as={Row} className="p-2">
      <Form.Label column sm={2}>
        {settingInfo.displayName}
      </Form.Label>

      <Col sm={10}>
        <InputElement
          id={elementId}
          value={value ?? settingInfo.defaultValue}
          onChange={(newValue) => {
            setValue(newValue)
          }}
        />
        <Form.Text>{settingInfo.description}</Form.Text>
      </Col>
    </Form.Group>
  )
}
