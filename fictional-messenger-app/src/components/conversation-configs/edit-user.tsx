import { type SettingInfo, type User } from 'fictional-messenger-renderer'
import { type PrimitiveAtom, useAtom, useAtomValue } from 'jotai'
import React, { JSX } from 'react'
import {
  AccordionBody,
  AccordionHeader,
  AccordionItem,
  Col,
  Form,
} from 'react-bootstrap'
import { SettingsForm } from './settings-form.tsx'
import { useAtomAt, useAtomProp } from '../../hooks/use-atom-prop.ts'

interface EditUserProps {
  usersAtom: PrimitiveAtom<User[]>
  userIndex: number
  settingsInfo: SettingInfo[]
  elementIdPrefix: string
}

export function EditUser({
  usersAtom,
  userIndex,
  settingsInfo,
  elementIdPrefix,
}: EditUserProps): JSX.Element {
  const userAtom = useAtomAt(usersAtom, userIndex)
  const user = useAtomValue(userAtom)

  const displayNameAtom = useAtomProp(userAtom, 'displayName')
  const [displayName, setDisplayName] = useAtom(displayNameAtom)

  const accordionKey = userIndex.toString()
  const displayNameId = `display-name-${user.name}`

  return (
    <AccordionItem eventKey={accordionKey}>
      <AccordionHeader>
        {user.name}
        {user.displayName != null && user.displayName.length > 0 && (
          <span className="fw-light text-body-secondary">
            <small>({user.displayName})</small>
          </span>
        )}
      </AccordionHeader>
      <AccordionBody>
        <Form.Group>
          <Form.Label sm={2}>Display Name</Form.Label>

          <Col sm={10}>
            <Form.Control
              type="text"
              id={displayNameId}
              name={displayNameId}
              value={displayName}
              onChange={(e) => {
                setDisplayName(e.target.value)
              }}
            />
            <Form.Text>
              How the user's name will appear in the conversation.
            </Form.Text>
          </Col>
        </Form.Group>

        <SettingsForm
          settingsAtom={useAtomProp(userAtom, 'settingValues')}
          settingsInfo={settingsInfo}
          elementIdPrefix={`${elementIdPrefix}-${user.name}`}
        />
      </AccordionBody>
    </AccordionItem>
  )
}
