import { type SettingInfo, type User } from 'fictional-messenger-renderer'
import { type PrimitiveAtom, useAtomValue } from 'jotai'
import React, { JSX } from 'react'
import { Accordion } from 'react-bootstrap'
import { EditUser } from './edit-user.tsx'

interface EditUsersProps {
  usersAtom: PrimitiveAtom<User[]>
  settingsInfo: SettingInfo[]
  elementIdPrefix: string
}

export function EditUsers({
  usersAtom,
  settingsInfo,
  elementIdPrefix,
}: EditUsersProps): JSX.Element {
  const users = useAtomValue(usersAtom)

  return (
    <Accordion defaultActiveKey={'0'}>
      {users.map((_, i) => (
        <EditUser
          key={`${elementIdPrefix}-${i}`}
          usersAtom={usersAtom}
          userIndex={i}
          settingsInfo={settingsInfo}
          elementIdPrefix={elementIdPrefix}
        />
      ))}
    </Accordion>
  )
}
