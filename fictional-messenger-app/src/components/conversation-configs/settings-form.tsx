import { type SettingInfo } from 'fictional-messenger-renderer'
import { type PrimitiveAtom } from 'jotai'
import React, { JSX } from 'react'
import { Form } from 'react-bootstrap'
import { SettingInput } from './setting-input.tsx'

interface SettingsFormProps {
  settingsAtom: PrimitiveAtom<Record<string, any>>
  settingsInfo: SettingInfo[]
  elementIdPrefix: string
}

export function SettingsForm({
  settingsAtom,
  settingsInfo,
  elementIdPrefix,
}: SettingsFormProps): JSX.Element {
  if (settingsInfo.length === 0) {
    return (
      <p>
        <em>No settings for this style.</em>
      </p>
    )
  }

  return (
    <Form>
      {settingsInfo.map((settingInfo, i) => (
        <SettingInput
          key={`${elementIdPrefix}-${i}`}
          settingInfo={settingInfo}
          settingsAtom={settingsAtom}
          elementIdPrefix={elementIdPrefix}
        />
      ))}
    </Form>
  )
}
