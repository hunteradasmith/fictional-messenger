import React, { JSX, useState } from 'react'
import { type SettingInputElementProps } from './setting-input.tsx'
import { type HslColor, HslColorPicker } from 'react-colorful'
import {
  type HslColor as FmHslColor,
  Settings,
} from 'fictional-messenger-renderer'
import lodash from 'lodash'
import convert from 'color-convert'
import {
  Button,
  Form,
  InputGroup,
  OverlayTrigger,
  Popover,
} from 'react-bootstrap'

function fmColorToPickerColor(fmColor: FmHslColor): HslColor {
  return { h: fmColor.hue, s: fmColor.sat, l: fmColor.lit }
}

export function SettingColorPicker({
  value,
  onChange,
}: SettingInputElementProps): JSX.Element {
  const valueColor: FmHslColor = value as FmHslColor
  const valueColorString = `hsl(${valueColor.hue}, ${valueColor.sat}%, ${valueColor.lit}%)`

  const [innerPickerColor, setInnerPickerColor] = useState<HslColor>(
    fmColorToPickerColor(valueColor),
  )
  const [innerHexColor, setInnerHexColor] = useState<string>(() =>
    convert.hsl.hex(valueColor.hue, valueColor.sat, valueColor.lit),
  )
  const [hexColorValid, setHexColorValid] = useState<boolean>(true)

  const innerOnChange = (newColor: FmHslColor, updatePicker = false): void => {
    const newRoundedColor: FmHslColor = Settings.Utils.nearestColor(newColor)

    if (!lodash.isEqual(valueColor, newRoundedColor)) {
      onChange(newRoundedColor)

      setInnerHexColor(
        convert.hsl.hex(
          newRoundedColor.hue,
          newRoundedColor.sat,
          newRoundedColor.lit,
        ),
      )
      setHexColorValid(true)

      if (updatePicker) {
        setInnerPickerColor(fmColorToPickerColor(newRoundedColor))
      }
    }
  }

  const pickerOnChange = (newPickerColor: HslColor): void => {
    setInnerPickerColor(newPickerColor)
    innerOnChange({
      hue: newPickerColor.h,
      sat: newPickerColor.s,
      lit: newPickerColor.l,
    })
  }

  const onSetHexColor = (): void => {
    if (innerHexColor.match(/[a-f\d]{6}|[a-f\d]{3}/i)) {
      setHexColorValid(true)
      const hsl = convert.hex.hsl(innerHexColor)

      innerOnChange(
        {
          hue: hsl[0],
          sat: hsl[1],
          lit: hsl[2],
        },
        true,
      )
    } else {
      setHexColorValid(false)
    }
  }

  const popover = (
    <Popover>
      <Popover.Body className="d-flex flex-column align-items-center">
        <HslColorPicker color={innerPickerColor} onChange={pickerOnChange} />
        <InputGroup style={{ maxWidth: '15em' }} className="my-2">
          <InputGroup.Text>Hex</InputGroup.Text>
          <Form.Control
            type="text"
            value={innerHexColor}
            onChange={(e) => setInnerHexColor(e.target.value)}
            onKeyDown={(e) => {
              if (e.key === 'Enter') onSetHexColor()
            }}
            isInvalid={!hexColorValid}
          />
          <Button variant="outline-secondary" onClick={onSetHexColor}>
            Set
          </Button>
        </InputGroup>
        <p className="text-muted">
          <small>Colors are rounded to nearest available.</small>
        </p>
      </Popover.Body>
    </Popover>
  )

  return (
    <div className="setting-color-picker">
      <OverlayTrigger
        placement="right"
        trigger="click"
        rootClose={true}
        overlay={popover}
      >
        <Button
          className="swatch-button"
          style={{ backgroundColor: valueColorString }}
        >
          &nbsp;
        </Button>
      </OverlayTrigger>
    </div>
  )
}
