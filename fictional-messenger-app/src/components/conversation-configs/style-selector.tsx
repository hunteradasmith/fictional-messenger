import React, { JSX, PropsWithChildren, useMemo, useState } from 'react'
import { type PrimitiveAtom, useAtom } from 'jotai'
import { Button, Card, Col, Modal, Row } from 'react-bootstrap'
import { styles, type StyleInfo } from 'fictional-messenger-renderer'
import { StyleDemo } from '../style-demo.tsx'

interface StyleSelectorProps {
  styleAtom: PrimitiveAtom<string>
}

function SelectorStyleInfo({
  styleInfo,
  demo,
  children,
}: PropsWithChildren<{
  styleInfo: StyleInfo
  demo?: boolean
}>): JSX.Element {
  return (
    <Card className="bg-light h-100">
      <Card.Body className="d-flex flex-column justify-content-between gap-2">
        <Row className="flex-grow-1">
          <Col xs={12} md={demo ? 4 : 12}>
            <Card.Title>{styleInfo.displayName}</Card.Title>
            <Card.Text>{styleInfo.description}</Card.Text>
          </Col>
          {demo ? (
            <Col xs={12} md={8}>
              <StyleDemo style={styleInfo.name} size="sm" />
            </Col>
          ) : null}
        </Row>
        <div className="d-grid w-100">{children}</div>
      </Card.Body>
    </Card>
  )
}

function SelectorModal({
  show,
  onHide,
  onSelect,
}: {
  show: boolean
  onHide: () => void
  onSelect: (style: string) => void
}): JSX.Element {
  const sortedStyles = useMemo(
    () =>
      Object.values(styles.infoByName).sort((a, b) =>
        a.displayName.localeCompare(b.displayName),
      ),
    [],
  )

  return (
    <Modal show={show} onHide={onHide}>
      <Modal.Header closeButton>Select Style</Modal.Header>
      <Modal.Body>
        <Row xs={1} className="gap-3">
          {sortedStyles.map((styleInfo) => (
            <Col key={styleInfo.name}>
              <SelectorStyleInfo styleInfo={styleInfo} demo={true}>
                <Button
                  variant="primary"
                  onClick={() => {
                    onSelect(styleInfo.name)
                    onHide()
                  }}
                >
                  Select
                </Button>
              </SelectorStyleInfo>
            </Col>
          ))}
        </Row>
      </Modal.Body>
    </Modal>
  )
}

export function StyleSelector({ styleAtom }: StyleSelectorProps): JSX.Element {
  const [style, setStyle] = useAtom(styleAtom)
  const [showModal, setShowModal] = useState(false)

  return (
    <div>
      <SelectorStyleInfo styleInfo={styles.infoByName[style ?? 'basic']}>
        <Button variant="primary" onClick={() => setShowModal(true)}>
          Select Style...
        </Button>
      </SelectorStyleInfo>
      <SelectorModal
        show={showModal}
        onHide={() => setShowModal(false)}
        onSelect={setStyle}
      />
    </div>
  )
}
