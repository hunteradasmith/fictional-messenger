import { styles } from 'fictional-messenger-renderer'
import React, { JSX } from 'react'

interface PreviewProps {
  html: string
  height?: number
  additionalRules?: string
  id?: string
  className?: string
}

export function Preview({
  html,
  height,
  additionalRules,
  id,
  className,
}: PreviewProps): JSX.Element {
  const previewHtml = `
    <style>
      ${styles.conversationCss}
      html, body { font-family: sans-serif; }
      ${additionalRules}
    </style>
    ${html}`.replaceAll('\n', '')

  return (
    <iframe
      id={id}
      className={className}
      height={height}
      srcDoc={previewHtml}
    />
  )
}
