import React, { JSX } from 'react'
import { Button, ButtonGroup } from 'react-bootstrap'
import {
  type AppStateEvent,
  useAppEventDispatcher,
} from '../state/app-state.ts'

interface StateEventButtonProps {
  buttons: { event: AppStateEvent; label: string; color?: string }[]
}

export function StateEventButtons({
  buttons,
}: StateEventButtonProps): JSX.Element {
  const eventDispatcher = useAppEventDispatcher()

  return (
    <ButtonGroup size="lg" className="w-100 my-5">
      {buttons.map((button, i) => (
        <Button
          key={`${i}-${button.label}`}
          variant={button.color ?? 'primary'}
          size="lg"
          className="flex-grow-1"
          onClick={() => {
            eventDispatcher(button.event)
          }}
        >
          {button.label}
        </Button>
      ))}
    </ButtonGroup>
  )
}
