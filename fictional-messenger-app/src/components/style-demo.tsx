import {
  type ConversationConfig,
  parseConversation,
  renderConversation,
} from 'fictional-messenger-renderer'
import React, { JSX, useEffect, useState } from 'react'
import { Preview } from './preview.tsx'
import fmLogo from 'data-url:../../img/icon-purple-bg.svg' // eslint-disable-line

export const styleDemoConversationText = `fm: Welcome to Fictional Messenger!
af: Make fancy chats like this,
without messing with HTML.`

const demoSettingValuesByStyle: Record<
  string,
  {
    settings?: Record<string, any>
    userSettings?: Record<string, Record<string, any>>
  }
> = {
  sms: {
    userSettings: {
      fm: {
        primary: true,
      },
    },
  },
  discordant: {
    userSettings: {
      fm: {
        profileImageUrl: fmLogo,
        nameColor: { hue: 265, sat: 100, lit: 70 },
      },
      af: {
        profileImageUrl: 'https://picsum.photos/id/69/200/200',
        nameColor: { hue: 195, sat: 100, lit: 70 },
      },
    },
  },
}

export function StyleDemo({
  style,
  size,
}: {
  style: string
  size?: 'lg' | 'sm'
}): JSX.Element {
  const [html, setHtml] = useState('')

  useEffect(() => {
    const demoSettingValues = demoSettingValuesByStyle[style] ?? {}
    const demoConfig: ConversationConfig = {
      style: style,
      settingValues: demoSettingValues?.settings ?? {},
      users: [
        {
          name: 'fm',
          displayName: 'Fictional Messenger',
          settingValues: demoSettingValues?.userSettings?.fm ?? {},
        },
        {
          name: 'af',
          displayName: 'Also Fictional Messenger',
          settingValues: demoSettingValues?.userSettings?.af ?? {},
        },
      ],
    }

    const { conversation, config } = parseConversation(
      styleDemoConversationText,
      'plain',
      demoConfig,
    )
    const result = renderConversation(conversation, config)

    setHtml(result.html)
  }, [style])

  let height = 275
  let fontSize = 1

  if (size === 'sm') {
    height = 175
    fontSize = 0.8
  }

  const demoAdditonalRules = `html, body { font-size: ${fontSize}em; height: 100%; margin: 0; background-color: white; overflow: clip; }
  blockquote.fm-conversation { width: 100vw; height: 100vh; }
  blockquote.fm-conversation-style-basic { padding: 0.5em; }
  .messages-container { height: 100% }`

  return (
    <Preview
      html={html}
      className="w-100"
      height={height}
      additionalRules={demoAdditonalRules}
    />
  )
}
