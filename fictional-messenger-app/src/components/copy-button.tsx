import React, { JSX, useEffect, useState } from 'react'
import { Button } from 'react-bootstrap'
import { Clipboard } from 'react-feather'

export function CopyButton({ text }: { text: string }): JSX.Element {
  const [copied, setCopied] = useState(false)

  useEffect(() => {
    if (copied) {
      const timeoutHandle = setTimeout(() => setCopied(false), 3000)
      return () => clearTimeout(timeoutHandle)
    }
  }, [copied])

  return (
    <Button
      variant={copied ? 'dark' : 'outline-secondary'}
      disabled={copied}
      onClick={() => {
        setCopied(true)
        navigator.clipboard.writeText(text)
      }}
    >
      <Clipboard />
      {copied ? 'Copied' : 'Copy'}
    </Button>
  )
}
