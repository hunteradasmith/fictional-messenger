import ReactDOMClient from 'react-dom/client'
import { AppContainer } from './pages/app-container.tsx'

document.addEventListener('DOMContentLoaded', () => {
  const container = document.getElementById('app-container')

  if (container === null) {
    throw Error('Could not get app-container element')
  }

  if (container.hasChildNodes()) {
    ReactDOMClient.hydrateRoot(container, AppContainer())
  } else {
    ReactDOMClient.createRoot(container).render(AppContainer())
  }
})
