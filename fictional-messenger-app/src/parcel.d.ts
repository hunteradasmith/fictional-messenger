// Permits Parcel's data-url: imports
declare module 'data-url:*' {
  export default string
}
