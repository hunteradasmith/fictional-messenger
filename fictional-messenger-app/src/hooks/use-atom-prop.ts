import { type PrimitiveAtom } from 'jotai'
import { focusAtom } from 'jotai-optics'
import { useMemo } from 'react'

/**
 * React hook that uses {@link jotai#focusAtom} to return a dependent atom which
 * contains and sets the value of the specified property.
 *
 * @param atom - {@link jotai#Atom} from which the new dependent atom will be created
 * @param prop - name of property on the atom which will be focused upon
 * @returns a new atom focused on the specified property of the provided atom
 */
export function useAtomProp<T, TProp extends keyof T>(
  atom: PrimitiveAtom<T>,
  prop: TProp,
): PrimitiveAtom<T[TProp]> {
  return useMemo(
    () => focusAtom(atom, (optic) => optic.prop(prop)),
    [atom, prop],
  )
}

/**
 * React hook that uses {@link jotai#focusAtom} to return a dependent atom which
 * contains and sets the value of the specified array index.
 *
 * @param atom - {@link jotai#Atom} from which the new dependent atom will be created
 * @param index - array index on the atom which will be focused upon
 * @returns a new atom focused on the specified array index of the provided atom
 */
export function useAtomAt<T, N extends number>(
  atom: PrimitiveAtom<T[]>,
  index: N,
): PrimitiveAtom<T> {
  return useMemo(
    () => focusAtom(atom, (optic) => optic.at(index)) as PrimitiveAtom<T>,
    [atom, index],
  )
}
