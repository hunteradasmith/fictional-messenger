import React, { JSX, StrictMode } from 'react'
import { AppStatePageSelector } from './app-state-page-selector.tsx'
import { Badge, Container, Nav, Navbar } from 'react-bootstrap'
import { fmUrls } from '../urls.ts'
import { ExternalLink } from 'react-feather'
import fmLogo from 'data-url:../../img/icon-no-bg.svg' // eslint-disable-line

export function AppContainer(): JSX.Element {
  return (
    <StrictMode>
      <Navbar collapseOnSelect expand="sm" bg="primary" data-bs-theme="dark">
        <Container fluid>
          <Navbar.Brand href="#">
            <img
              aria-hidden="true"
              src={fmLogo}
              width="30"
              height="30"
              className="d-inline-block align-top"
            />
            {' Fictional Messenger '}
            <Badge bg="danger">Alpha</Badge>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="app-navbar" />
          <Navbar.Collapse id="app-navbar">
            <Nav className="ms-auto">
              <Nav.Link href={fmUrls.docs} target="_blank">
                Docs
                <ExternalLink />
              </Nav.Link>
              <Nav.Link href={fmUrls.discord} target="_blank">
                Discord
                <ExternalLink />
              </Nav.Link>
              <Nav.Link href={fmUrls.gitlab} target="_blank">
                GitLab
                <ExternalLink />
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      <AppStatePageSelector />
    </StrictMode>
  )
}
