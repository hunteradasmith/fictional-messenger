import React, { JSX, useEffect } from 'react'
import { useAtomValue, useSetAtom } from 'jotai'
import { useAppEventDispatcher } from '../state/app-state.ts'
import {
  conversationsTextAtom,
  conversationCollectionAtom,
  conversationsFormatAtom,
  conversationsErrorMessageAtom,
} from '../state/conversations.ts'
import { Loading } from '../components/loading.tsx'
import { parseConversationCollection } from 'fictional-messenger-renderer'

export function ParseContent(): JSX.Element {
  const eventDispatcher = useAppEventDispatcher()

  const format = useAtomValue(conversationsFormatAtom)
  const text = useAtomValue(conversationsTextAtom)

  const setConversationCollection = useSetAtom(conversationCollectionAtom)
  const setConversationErrorMessage = useSetAtom(conversationsErrorMessageAtom)

  useEffect(() => {
    try {
      // TODO this is usually fast, but an async method or service worker couldn't hurt
      const coll = parseConversationCollection(text, format)
      setConversationCollection(coll)

      eventDispatcher('success')
    } catch (e: unknown) {
      console.error(e)

      if (e instanceof Error) {
        /* TODO this should use `instanceof ConversationParseError`, but TypeScript is doing
           weird things during the compile causing that to throw an exception at runtime */
        if (Object.hasOwn(e, 'lineNumber')) {
          setConversationErrorMessage(
            `Line ${(e as any).lineNumber as number}: ${e.message}`,
          )
        } else {
          setConversationErrorMessage(e.message)
        }
      } else {
        setConversationErrorMessage('An unknown error occurred.')
      }

      eventDispatcher('error')
    }
  }, [text])

  return <Loading />
}
