import React, { JSX, useEffect } from 'react'
import { useAtomValue, useSetAtom } from 'jotai'
import { useAppEventDispatcher } from '../state/app-state.ts'
import {
  conversationCollectionAtom,
  conversationsErrorMessageAtom,
  renderResultAtom,
} from '../state/conversations.ts'
import { Loading } from '../components/loading.tsx'
import { renderConversationCollection } from 'fictional-messenger-renderer'

export function RenderConversations(): JSX.Element {
  const eventDispatcher = useAppEventDispatcher()
  const conversationCollection = useAtomValue(conversationCollectionAtom)

  const setRenderResult = useSetAtom(renderResultAtom)
  const setConversationErrorMessage = useSetAtom(conversationsErrorMessageAtom)

  useEffect(() => {
    if (conversationCollection === null) {
      throw new Error('conversationCollection is null')
    }

    try {
      const result = renderConversationCollection(conversationCollection)
      setRenderResult(result)
      eventDispatcher('success')
    } catch (e: unknown) {
      console.error(e)

      if (e instanceof Error) {
        /* TODO this should use `instanceof ConversationParseError`, but TypeScript is doing
           weird things during the compile causing that to throw an exception at runtime */
        if (Object.hasOwn(e, 'lineNumber')) {
          setConversationErrorMessage(
            `Line ${(e as any).lineNumber as number}: ${e.message}`,
          )
        } else {
          setConversationErrorMessage(e.message)
        }
      } else {
        setConversationErrorMessage('An unknown error occurred.')
      }

      eventDispatcher('error')
    }
  }, [conversationCollection])

  return <Loading />
}
