import React, { JSX, useEffect } from 'react'
import { type AppState, appStateAtom } from '../state/app-state.ts'
import { useAtomValue } from 'jotai'
import { Intro } from './intro.tsx'
import { InputContent } from './input-content.tsx'
import { ParseContent } from './parse-content.tsx'
import { EditConversationConfigs } from './edit-conversation-configs.tsx'
import { RenderConversations } from './render-conversations.tsx'
import { ShowResult } from './show-results.tsx'
import { ParseContentError } from './parse-content-error.tsx'
import { RenderConversationsError } from './render-conversations-error.tsx'

const ComponentsByState: Record<AppState, React.FC> = {
  intro: Intro,
  'input-content': InputContent,
  'parse-content': ParseContent,
  'parse-content-error': ParseContentError,
  'edit-conversation-configs': EditConversationConfigs,
  'render-conversations': RenderConversations,
  'render-conversations-error': RenderConversationsError,
  'show-results': ShowResult,
}

const noAnalyticsStates: AppState[] = ['intro']

export function AppStatePageSelector(): JSX.Element {
  const state = useAtomValue(appStateAtom)

  useEffect(() => {
    window.scrollTo({ top: 0, left: 0, behavior: 'instant' })

    if (
      globalThis['plausible'] !== undefined &&
      !noAnalyticsStates.includes(state)
    ) {
      globalThis.plausible('app-state', { props: { state } })
    }
  }, [state])

  const Component = ComponentsByState[state]
  return <Component />
}
