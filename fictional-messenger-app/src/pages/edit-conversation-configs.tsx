import React, { JSX } from 'react'
import { useAtomValue } from 'jotai'
import { conversationCollectionAtom } from '../state/conversations.ts'
import { EditConversationConfig } from '../components/conversation-configs/edit-conversation-config.tsx'
import { useAtomProp } from '../hooks/use-atom-prop.ts'
import { StateEventButtons } from '../components/state-event-buttons.tsx'
import { Alert } from 'react-bootstrap'

export function EditConversationConfigs(): JSX.Element {
  const conversationConfigsAtom = useAtomProp(
    conversationCollectionAtom,
    'configsByName',
  )
  const conversationConfigs = useAtomValue(conversationConfigsAtom)
  const hasConfigs = Object.keys(conversationConfigs).length > 0

  return (
    <div className="container mt-3">
      <h1>Configure Your Conversations</h1>
      <p className="lead">
        Set settings to control how your conversations will appear.
      </p>
      {hasConfigs ? (
        Object.keys(conversationConfigs).map(
          (name: keyof typeof conversationConfigs) => (
            <EditConversationConfig
              key={name}
              conversationConfigName={name}
              conversationConfigAtom={useAtomProp(
                conversationConfigsAtom,
                name,
              )}
            />
          ),
        )
      ) : (
        <Alert variant="primary">
          No conversations were found. Please click 'Back' and double-check your
          syntax.
        </Alert>
      )}

      <StateEventButtons
        buttons={[
          { label: 'Back', event: 'prev', color: 'secondary' },
          { label: 'Next', event: 'next' },
        ]}
      />
    </div>
  )
}
