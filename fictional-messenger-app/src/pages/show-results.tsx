import { useAtomValue } from 'jotai'
import React, { JSX } from 'react'
import {
  conversationCollectionAtom,
  conversationsFormatAtom,
  renderResultAtom,
} from '../state/conversations.ts'
import { useAtomProp } from '../hooks/use-atom-prop.ts'
import { Col, Row } from 'react-bootstrap'
import { CodeBlock } from '../components/code-block.tsx'
import { StateEventButtons } from '../components/state-event-buttons.tsx'
import { styles } from 'fictional-messenger-renderer'
import { Preview } from '../components/preview.tsx'
import { fmUrls } from '../urls.ts'

const fmInfoHtml = `This work's chat conversations were created with <a href="${fmUrls.fm}?ref=us" target="_blank">Fictional Messenger</a>.`

export function ShowResult(): JSX.Element {
  const result = useAtomValue(renderResultAtom)
  const configsByNameAtom = useAtomProp(
    conversationCollectionAtom,
    'configsByName',
  )
  const configsByName = useAtomValue(configsByNameAtom)
  const format = useAtomValue(conversationsFormatAtom)

  let configPrefix: string, configSuffix: string

  switch (format) {
    case 'html':
      configPrefix = '--- fm-configs ---'
      configSuffix = '--- end ---'
      break
    case 'markdown':
      configPrefix = '```fm-configs'
      configSuffix = '```'
      break
  }

  const configsJson = `${configPrefix}\n${JSON.stringify(configsByName)}\n${configSuffix}`
  const cssLastModified = styles.conversationCssLastModified.toLocaleDateString(
    undefined,
    {
      day: 'numeric',
      month: 'short',
      year: 'numeric',
    },
  )

  return (
    <div className="container mt-3">
      <h1>Results</h1>

      <Row>
        <Col lg="6">
          <h4>Configuration</h4>
          <p className="text-muted">
            This is your configuration options for your conversations. Paste the
            below somewhere alongside your conversations to save your current
            configuration.
          </p>
          <CodeBlock value={configsJson} />

          <h4>HTML</h4>
          <p className="text-muted">
            This is the rendered version of your work, including conversations.
            If publishing to AO3, this is your chapter contents.
          </p>
          <CodeBlock value={result.html.replaceAll('\n', '')} />

          <h4>CSS</h4>
          <p className="text-muted">
            This is Fictional Messenger's style information. If publishing to
            AO3, this is your work skin.
          </p>
          <p>
            Last updated: <strong>{cssLastModified}</strong>
          </p>
          <CodeBlock value={styles.conversationCss} />

          <h4>Link</h4>
          <p className="text-muted">
            If you use Fictional Messenger in a work, consider leaving a link in
            your end notes.
          </p>
          <CodeBlock value={fmInfoHtml} />
        </Col>
        <Col lg="6">
          <h4>Preview</h4>
          <div className="border border-secondary rounded bg-light">
            <Preview html={result.html} id="preview-result" />
          </div>
        </Col>
      </Row>

      <StateEventButtons
        buttons={[
          { label: 'Back', event: 'prev', color: 'secondary' },
          { label: 'Start Over', event: 'reset', color: 'warning' },
        ]}
      />
    </div>
  )
}
