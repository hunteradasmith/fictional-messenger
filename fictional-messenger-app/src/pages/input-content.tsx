import React, { JSX, useEffect, useState } from 'react'
import { StateEventButtons } from '../components/state-event-buttons.tsx'
import { Alert, Col, Dropdown } from 'react-bootstrap'
import { useSetAtom } from 'jotai'
import {
  conversationsFormatAtom,
  conversationsTextAtom,
} from '../state/conversations.ts'

enum InputFormat {
  richText = 'richText',
  html = 'html',
  markdown = 'markdown',
}

const formatTitles: Record<InputFormat, string> = {
  [InputFormat.richText]: 'Rich Text',
  [InputFormat.html]: 'HTML',
  [InputFormat.markdown]: 'Markdown',
}

export function InputContent(): JSX.Element {
  const setFormat = useSetAtom(conversationsFormatAtom)
  const setText = useSetAtom(conversationsTextAtom)

  const [formatSelectorState, setFormatSelectorState] = useState<InputFormat>(
    InputFormat.richText,
  )

  useEffect(() => {
    switch (formatSelectorState) {
      case InputFormat.richText:
      case InputFormat.html:
        setFormat('html')
        break
      case InputFormat.markdown:
        setFormat('markdown')
    }
  }, [formatSelectorState])

  return (
    <div className="container mt-3">
      <h1>Paste Your Document</h1>
      <p className="lead">
        Copy a document containing at least one conversation, then paste it
        below.
      </p>
      <Alert color="warning">
        Note: Changes you make below won't be saved. Make any changes on the
        original document and then copy/paste again.
      </Alert>

      <Col xs={12} md={10} xl={8} className="mx-auto my-3">
        <Dropdown id="format-selector" className="mb-1">
          <Dropdown.Toggle variant="primary" size="lg">
            {formatTitles[formatSelectorState]}
          </Dropdown.Toggle>

          <Dropdown.Menu>
            {Object.values(InputFormat).map((format) => (
              <Dropdown.Item
                key={format}
                onClick={() => setFormatSelectorState(format)}
              >
                {formatTitles[format]}
              </Dropdown.Item>
            ))}
          </Dropdown.Menu>
        </Dropdown>

        {formatSelectorState === InputFormat.richText ? (
          <div
            className="input-content"
            contentEditable
            onInput={(e) => {
              setText(e.currentTarget.innerHTML)
            }}
          />
        ) : (
          <textarea
            className="input-content w-100"
            autoComplete="off"
            spellCheck="false"
            onInput={(e) => {
              setText(e.currentTarget.value)
            }}
          />
        )}
      </Col>

      <StateEventButtons
        buttons={[
          { label: 'Back', event: 'prev', color: 'secondary' },
          { label: 'Next', event: 'next' },
        ]}
      />
    </div>
  )
}
