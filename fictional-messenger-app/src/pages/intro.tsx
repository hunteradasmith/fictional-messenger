import React, { JSX } from 'react'
import {
  StyleDemo,
  styleDemoConversationText,
} from '../components/style-demo.tsx'
import { Alert, Button, Carousel, Col, Row } from 'react-bootstrap'
import { ExternalLink } from 'react-feather'
import { useAppEventDispatcher } from '../state/app-state.ts'
import { styles } from 'fictional-messenger-renderer'
import { fmUrls } from '../urls.ts'

export function Intro(): JSX.Element {
  const eventDispatcher = useAppEventDispatcher()

  return (
    <>
      <div className="container-fluid bg-light">
        <figure className="text-center m-0 py-5">
          <h3 className="display-3">Fictional Messenger</h3>
          <p className="lead text-muted">
            Easily add fancy group chats and text conversations to your fic or
            website.
          </p>

          <Col lg={6} className="my-5 mx-auto">
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Button
                variant="primary"
                size="lg"
                className="px-4 gap-3"
                onClick={() => eventDispatcher('next')}
              >
                Get Started
              </Button>
              <Button
                variant="outline-secondary"
                size="lg"
                className="px-4 gap-3"
                href={fmUrls.docs}
                target="_blank"
              >
                How-To Guide <ExternalLink />
              </Button>
            </div>

            <div className="mt-5">
              <Alert variant="warning" className="text-center">
                <strong>Oops.</strong> AO3's work skin system currently doesn't
                support the method we use to apply colors, so colors don't
                currently work correctly there. We're working on resolving the
                situation.
              </Alert>

              <p>
                We're still in alpha, so there will be bugs, and many planned
                features and chat styles aren't available yet.
              </p>

              <p>
                Have questions or comments?
                <a href={fmUrls.discord}>{' Join the Discord.'}</a>
              </p>
            </div>
          </Col>
        </figure>
      </div>

      <div className="container-fluid bg-dark">
        <figure className="m-0 py-5">
          <Row xs={1} md={2} xxl={4} className="justify-content-center">
            <Col className="my-3">
              <h4 className="text-center text-light mb-4">Turn this...</h4>
              <pre className="code-block bg-light p-3">
                <code>{styleDemoConversationText}</code>
              </pre>
            </Col>
            <Col className="my-3">
              <h4 className="text-center text-light mb-4">...into this.</h4>
              <Carousel controls={false} data-bs-theme="dark">
                {Object.values(styles.infoByName).map((styleInfo) => (
                  <Carousel.Item key={styleInfo.name}>
                    <StyleDemo style={styleInfo.name} />
                  </Carousel.Item>
                ))}
              </Carousel>
            </Col>
          </Row>
        </figure>
      </div>

      <div className="container-fluid bg-light">
        <figure className="m-0 py-5">
          <Row xs={1} md={3} xl={6} className="justify-content-center">
            <Col>
              <h5>Simple to use</h5>
              <p>
                No more messing with complicated HTML. Write your story normally
                and add chat conversations in a straightforward format.
                Fictional Messenger will do the rest.
              </p>
            </Col>
            <Col>
              <h5>Features</h5>
              <ul>
                <li>
                  Works seamlessly with AO3 to display chats as actual text, not
                  just embedded images. Chat messages are still available for
                  readers with work skins are disabled, and those using a screen
                  reader.
                </li>
                <li>
                  Lets you type short usernames, then display them as long ones.
                </li>
                <li>
                  Supports multiple named chat configs, so you can easily use
                  different chat styles in the same work.
                </li>
              </ul>
            </Col>
            <Col>
              <h5>Contribute</h5>
              <p>
                Fictional Messenger is free, open-source software. Know CSS or
                JavaScript and want to add new styles, improve existing ones, or
                add new features?
                <a href={fmUrls.gitlab} target="_blank">
                  {' Check out the project on GitLab'}
                </a>
                , or
                <a href={fmUrls.discord} target="_blank">
                  {' join the Discord'}
                </a>
                .
              </p>
            </Col>
          </Row>
        </figure>
      </div>
    </>
  )
}
