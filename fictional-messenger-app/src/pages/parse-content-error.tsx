import React, { JSX } from 'react'
import { useAtomValue } from 'jotai'
import { conversationsErrorMessageAtom } from '../state/conversations.ts'
import { StateEventButtons } from '../components/state-event-buttons.tsx'
import { Alert } from 'react-bootstrap'

export function ParseContentError(): JSX.Element {
  const conversationsErrorMessage = useAtomValue(conversationsErrorMessageAtom)

  return (
    <div className="container mt-3">
      <Alert color="danger">
        <h1>Error</h1>
        <p>There was an error while parsing your conversations text:</p>
        <p>
          <strong>{conversationsErrorMessage}</strong>
        </p>

        <StateEventButtons buttons={[{ label: 'Back', event: 'prev' }]} />
      </Alert>
    </div>
  )
}
