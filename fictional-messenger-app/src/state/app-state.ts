/**
 * @privateRemarks
 * The app's overall state is handled by a simple state machine. If the app's complexity grows,
 * this should get switched to a more featureful state machine library like XState.
 */

import { atom, useAtom } from 'jotai'

export type AppState =
  | 'intro'
  | 'input-content'
  | 'parse-content'
  | 'parse-content-error'
  | 'edit-conversation-configs'
  | 'render-conversations'
  | 'render-conversations-error'
  | 'show-results'

export type AppStateEvent = 'next' | 'prev' | 'success' | 'error' | 'reset'

export const AppStateRules: Record<
  AppState,
  Partial<Record<AppStateEvent, AppState>>
> = {
  intro: { next: 'input-content' },
  'input-content': { prev: 'intro', next: 'parse-content' },
  'parse-content': {
    success: 'edit-conversation-configs',
    error: 'parse-content-error',
  },
  'parse-content-error': { prev: 'input-content' },
  'edit-conversation-configs': {
    prev: 'input-content',
    next: 'render-conversations',
  },
  'render-conversations': {
    success: 'show-results',
    error: 'render-conversations-error',
  },
  'render-conversations-error': { prev: 'edit-conversation-configs' },
  'show-results': { prev: 'edit-conversation-configs', reset: 'intro' },
}

export const appStateAtom = atom<AppState>(
  Object.keys(AppStateRules)[0] as AppState,
)

export function useAppEventDispatcher(): (event: AppStateEvent) => void {
  const [currentState, setState] = useAtom(appStateAtom)
  const rules = AppStateRules[currentState]

  return (event: AppStateEvent) => {
    const nextState = rules[event]

    if (typeof nextState === 'undefined') {
      throw new Error(
        `App state "${currentState}" does not handle event "${event}"`,
      )
    }

    setState(nextState)
  }
}
