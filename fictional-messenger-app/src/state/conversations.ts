import {
  type ConversationCollection,
  type RenderResult,
} from 'fictional-messenger-renderer'
import { atom } from 'jotai'

export const conversationsFormatAtom = atom<'html' | 'markdown'>('html')
export const conversationsTextAtom = atom<string>('')
export const conversationsErrorMessageAtom = atom<string>('')

// These probably should be nullable and default to null, but that causes a types mess with `useAtomProp`
/* eslint @typescript-eslint/consistent-type-assertions: "off" */
export const conversationCollectionAtom = atom<ConversationCollection>(
  {} as ConversationCollection,
)
export const renderResultAtom = atom<RenderResult>({} as RenderResult)

conversationsTextAtom.debugLabel = 'conversationsText'
conversationCollectionAtom.debugLabel = 'conversationCollections'
renderResultAtom.debugLabel = 'renderResult'
