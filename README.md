# Fictional Messenger

Fictional Messenger makes it easy to display fictional chats & text messages with the appearance of real-world messaging platforms. It is intended for use in fiction where writers want to display conversations as if they were happing via text messages or chat apps.

* [Open Fictional Messenger](https://fictional.tools/messenger/?ref=gl)
* [Read the Guide](https://fictional.tools/messenger/docs/)

### Current Status

Fictional Messenger is in beta stage. The basic features work, but more planned features and conversation styles are still in development.


## Development info

This repository is a monorepo. Here's a quick map of its contents:

* [docs](docs/): Fictional Messenger's documentation
* [fictional-messenger-app](fictional-messenger-app/): The web application
* [fictional-messenger-cli](fictional-messenger-cli/): A command line version of the renderer
* [fictional-messenger-renderer](fictional-messenger-renderer): The renderer module used by the app and CLI versions.

Check out the README.md files of each of the above for more info.

# Examples
(Note: these examples appear as screenshots since GitLab doesn't permit CSS in their Markdown pages. Check out Fictional Messenger itself for live HTML.)

### SMS Style

![screenshot demonstrating the SMS style](examples/img/screenshots/sms.png)
Adapted quote from Terry Pratchett, _Going Postal._

* [this example's conversation text](examples/sms.txt)
* [this example's conversation config](examples/sms.json)

### Discordant Style

![screenshot demonstrating the Discordant style](examples/img/screenshots/discordant.png)
Adapted quote from Terry Pratchett, _A Blink of the Screen: Collected Shorter Fiction._

* [this example's conversation text](examples/discordant.txt)
* [this example's conversation config](examples/discordant.json)