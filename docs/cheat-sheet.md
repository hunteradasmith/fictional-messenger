## Cheat Sheet

- Start conversations with `-- fm: your-config-name --` and end them with `-- end --`
    - Use any number of dashes or hyphens
    - Config names can be anything, but can't have spaces
    - If you want multiple conversations to look the same as each other, use the same config name
- Messages are just a username, a colon, and the message text: `username: Message goes here.`
    - Messages from the same user can go on following lines without a username
- Blank lines are fine
- Lines starting with `//` are comments and won't appear in the output

#### Example

```text
-- fm: your-config-name --
username: Message goes here.
Messages from the same user can go on following lines.

other-username: Blank lines are fine.

// This comment won't appear in the output.
-- end --
```