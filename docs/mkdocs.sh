#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
REPO_DIR=$(dirname "$SCRIPT_DIR")
VENV_DIR=$REPO_DIR/.mkdocs-venv

python3 -m venv $VENV_DIR
source $VENV_DIR/bin/activate
python3 -m pip install -r $SCRIPT_DIR/requirements.txt

python3 -m mkdocs $1 --config-file $REPO_DIR/mkdocs.yml

deactivate