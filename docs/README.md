# Fictional Messenger Guide

Fictional Messenger makes it easy to display fictional chats & text messages with the appearance of real-world messaging platforms. It is intended for use in fiction where writers want to display conversations as if they were happing via text messages or chat apps.

Instead of generating static images like similar tools, Fictional Messenger generates HTML and CSS that can be used on [Archive Of Our Own](https://archiveofourown.org/), or anywhere else where HTML is permitted.

The HTML that Fictional Messenger generates will still be readable even when CSS isn't available, such as when AO3 work skins are disabled, or for those using screen readers.

Fictional Messenger will always be 100% free and open source.

We're in beta, so if you run into problems or have questions please [let us know in the Discord.](https://discord.gg/Q2bgvrjkCn)

## Writing Your Chat Conversations

Write your work how you normally do in any common editor like Google Docs, LibreOffice Writer, or Microsoft Word. When you want to add a chat conversation to your work, write it like this:

```text
-- fm: your-config-name --
jane: This is a message from Jane
doe: And this is a message from Doe
-- end --
```

Conversations should start with `-- fm: your-config-name --`, and end with `-- end --` This is what tells Fictional Messenger what part of your work contains chat messages. (The `--` parts can be any number of hyphens, dashes, or minus signs, so it's okay if your editor turns `--` into `—`.)

Put anything you want in the `your-config-name` part, except for spaces. This tells Fictional Messenger which "conversation config" to use. (You'll create those later.)

If you use the same config name for multiple chat conversations, then they'll use the same settings. This lets you make multiple conversations with the same users and appearance without needing to set up each one separately.

#### Messages

Inside the conversation, messages start with a username and a colon, followed by any text.

```text
username: Message goes here.
```

The username part can be a short name for that person. When you set up your conversation config later, you can give a different name for that person. For example, you could type `esmerelda:` and then have it appear as "Princess Esmerelda Margaret Note Spelling of Lancre."

Usernames can contain any characters expect for spaces and colons.

If writing multiple messages from the same user, you don't need to repeat the username. Just write messages on the following lines.

```text
jane: This is a message from Jane
This is another message from Jane
```

If you want to write a note to yourself inside a conversation without it appearing in your work, just start a line with two forward slashes.

```text
// This is a comment. It won't appear in your work.
jane: This is a message.
```

Also, you can put blank lines anywhere to keep long conversations easier to read. The blank lines won't appear in your work.

```text
jane: This is a message.

doe: This is a message after a blank line.
```

<div class="alert alert-info" role="alert">
We're still working on adding common chat features, like message timestamps and embedded images.
</div>

#### Using Markdown

Particularly nerdy authors may choose to write their works using [Markdown](https://commonmark.org/help/) instead of using a regular editor.

When using Markdown, write your conversations inside code fences like this:

``````markdown
```fm: your-config-name
jane: This is a message inside Markdown.
```
``````

In Markdown mode, Fictional Messenger supports Markdown inside of chat messages, too:

``````markdown
```fm: your-config-name
jane: You _can_ use [Markdown](https://commonmark.org/help/) in **messages**.
``````

## Using Fictional Messenger

Once you've created a document with some chat conversations in it, select the whole thing and copy it. Then open [Fictional Messenger](https://fictional.tools/messenger/?ref=docs) and click the "Get Started" button.

On the "Paste Your Document" page, paste your document into the text box. (Leave the format selector as "Rich Text", unless you're writing your work as plain HTML or Markdown.)

Click "Next". On the "Configure Your Conversations" page, you can change how your conversation will appear. Each "config name" you used inside a `-- fm: config-name --` will have its own section on this page.

Choose which style you want your conversations to use by clicking the "Select Style..." button. You'll see a list of styles with descriptions and previews. Click "Select" by the one you want.

<div class="alert alert-info" role="alert">
We're still working on adding more styles for common messaging platforms.
</div>

Each style has its own selection of settings to control how your conversations will appear. Settings under the "Settings" subsection will apply to the whole conversation. User-specific settings appear under each user's name in the "Users" section.

Under each user's name, you can set that user's "Display Name". This is the name that will appear in your conversation, letting you type a short username and then have it appear as something longer.

Once you've customized each "conversation config", click "Next". You'll be taken to the "Results" page, containing the finished version of your work that can be used in AO3 or other websites.

Under the "Configuration" subsection, make sure to copy your configuration and then paste it into your original document. This way, if you need to make changes to your work, Fictional Messenger will remember your conversation config settings so you don't need to enter them again.

(The `--- fm-configs ---` section can go anywhere in your document. Fictional Messenger will automatically detect it, and it won't appear in your finished work.)

If you later make more changes to your conversation configs, just replace the old `--- fm-configs ---` section with the new one.

## Publishing

Now that you've rendered your work to HTML, you'll need to publish it. How you do that depends on what you're publishing to.

### Archive Of Our Own (AO3)

Publishing to AO3 is pretty simple. Start by creating or editing a work on AO3 like usual. When it's time to input your work, copy the "HTML" section of Fictional Messenger's "Results" page and paste it into the "Work Text" section of your work on AO3.

Conversations in your will now appear, but only as regular text. To make your conversations appear in your chosen styles, you'll need to create a new work skin. You can do so from the "Skins" > "My Work Skins" section of your AO3 settings. (Check out [AO3's Guide to Work Skins](https://archiveofourown.org/admin_posts/1370) for detailed instructions.)

Set the work skin's title and description to whatever you want. Your work's name and "Fictional Messenger" would be a reasonable choice.

Copy Fictional Messenger's CSS from the "CSS" section of your "Results" page, and then paste it into the "CSS" section of AO3's "Create Work Skin" page. Then, click Save.

<div class="alert alert-info" role="alert">
If you use Fictional Messenger in multiple works, it's a good idea to use a separate skin for each one. Fictional Messenger's CSS may change over time, so using separate skins will ensure that new CSS changes don't cause problems for older works.<br />
<span class="text-small">(We'll try to avoid breaking changes either way, but this is still best practice.)</span>
</div>

Edit your work and select your new work skin. (Check out the AO3 guide above for detailed instructions.) After you save, you should see your conversations fully styled in your work.

If making changes or adding new chapters to an existing work that uses Fictional Messenger, be sure to pay attention to the "Last Updated" date listed in the "CSS" section of the "Results" page. If the updated date is later than the last modified date on your work skin, you should edit your skin and paste in the new CSS.

If you use Fictional Messenger in a work, consider leaving a link in your end notes.

```html
This work's chat conversations were created with <a href="https://fictional.tools/messenger/?ref=us" target="_blank">Fictional Messenger</a>.
```

### Other Sites

Other sites will have different steps for publishing your work. If they support HTML, then the "HTML" section of Fictional Messenger's "Results" page can be copied and pasted into that site.

If the site allows custom CSS, consult that site's documentation for instructions on setting that up.

If the site does not allow custom CSS then your conversations will appear in a plain format without styling, but should be perfectly readable.

### Plain HTML

If publishing to a plain HTML document, you can just paste in the "HTML" results wherever you'd like in the document. Be sure to include a `<style>` tag containing Fictional Messenger's CSS.