# fictional-messenger-cli

This module is a simple CLI tool for rendering Fictional Messenger conversations. It's currently intended more as a development tool than an end user program, though may be useful in the future for automated rendering in certain contexts.

# Building
First, the renderer module must be built using [the instructions in its README.](../fictional-messenger-renderer/README.md)

Next, build the CLI with:

    npm install
    npm run build


# Usage
Run the application with:
    node . [options] <conversation-path> <config-path>

### Arguments
* conversation-path: path to text file containing conversation
* config-path: path to JSON file containing conversation config

### Options
* -o, --output <output-path>: path to which rendered HTML will be written (defaults to stdout)
* -c, --css-output <css-output-path>: path to which rendered CSS will be written
* -i, --include-css-with-html: include CSS with generated HTML
* -h, --help: display help for command

# Examples

### Output to stdout
    node . examples/sms.txt examples/sms.json --include-css-with-html

### Output to file
    node . examples/sms.txt examples/sms.json --include-css-with-html --output sms.html
