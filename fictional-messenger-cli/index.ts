import { program } from 'commander'
import fs from 'node:fs/promises'
import process from 'node:process'
import { styles, parseConversation, renderConversation, type ConversationConfig } from 'fictional-messenger-renderer'

// TODO support rendering of conversation collections / HTML pages
program
  .name('fictional-messenger-cli')
  .description('Render text-based chat conversations to fancy HTML and CSS')
  .argument('<conversation-path>', 'path to text file containing conversation')
  .argument('<config-path>', 'path to JSON file containing conversation config')
  .option('-o, --output <output-path>', 'path to which rendered HTML will be written (defaults to stdout)')
  .option('-c, --css-output <css-output-path>', 'path to which rendered CSS will be written')
  .option('-i, --include-css-with-html', 'include CSS with generated HTML')
  .parse()

const options = program.opts()
const conversationPath = program.args[0]
const configPath = program.args[1]
const outputPath: string = options.output
const cssOutputPath: string = options.cssOutput
const includeCss: boolean = options.includeCssWithHtml

const conversationText = await fs.readFile(conversationPath, { encoding: 'utf8' })
const baseConfig: ConversationConfig = JSON.parse(await fs.readFile(configPath, { encoding: 'utf8' }))

const { conversation, config } = parseConversation(conversationText, 'plain', baseConfig)
const result = renderConversation(conversation, config)

let output = result.html
if (includeCss) {
  output += `<style>${styles.conversationCss}</style>`
}

if (outputPath !== undefined) {
  await fs.writeFile(outputPath, output)
} else {
  process.stdout.write(output + '\n')
}

if (cssOutputPath !== undefined) {
  await fs.writeFile(cssOutputPath, styles.conversationCss)
}
