/**
 * Describes an error related to a {@link ConversationConfig}
 */
export class ConversationConfigError extends Error {
  constructor(message: string) {
    super(message)

    this.name = 'ConversationConfigError'
  }
}
