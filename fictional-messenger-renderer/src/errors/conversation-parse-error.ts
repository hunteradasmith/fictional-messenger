/**
 * Describes an error related to parsing conversation text.
 */
export class ConversationParseError extends Error {
  /** Line number on which the error occurred. */
  public lineNumber: number | null

  constructor(message: string, lineNumber: number | null = null) {
    super(message)

    this.name = 'ConversationParseError'
    this.lineNumber = lineNumber
  }
}
