import { SettingInfo } from '../types/setting-info.ts'
import { SettingType } from '../types/setting-type.ts'
import { Settings } from '../types/settings.ts'

function inferSettingType(value: any): SettingType | null {
  if (typeof value === 'undefined' || value === null) {
    return null
  }

  switch (typeof value) {
    case 'string':
      return SettingType.String
    case 'number':
      return SettingType.Number
    case 'boolean':
      return SettingType.Boolean
    case 'object':
      break
    default:
      return null
  }

  if (['hue', 'sat', 'lit'].every((k) => Object.hasOwn(value, k))) {
    return SettingType.HslColor
  }

  return null
}

/**
 * Adds metadata to a setting property of a {@link Settings} class
 */
export function Setting(params: {
  displayName: string
  description: string
  type?: SettingType
}): <This extends Settings, Value>(
  target: ClassAccessorDecoratorTarget<This, Value>,
  context: ClassAccessorDecoratorContext<This, Value>,
) => ClassAccessorDecoratorResult<This, Value> {
  return function <This extends Settings, Value>(
    target: ClassAccessorDecoratorTarget<This, Value>,
    context: ClassAccessorDecoratorContext<This, Value>,
  ): ClassAccessorDecoratorResult<This, Value> {
    return {
      init(this: This, defaultValue: Value): Value {
        const settingType = params.type ?? inferSettingType(defaultValue)

        if (settingType === null) {
          throw new TypeError(
            `Could not determine type of setting ${String(context.name)}`,
          )
        }

        const settingInfo: SettingInfo = {
          name: String(context.name),
          displayName: params.displayName,
          description: params.description,
          defaultValue: defaultValue,
          type: settingType,
        }

        const value = this.registerSettingInfo(settingInfo)
        return value
      },
    }
  }
}
