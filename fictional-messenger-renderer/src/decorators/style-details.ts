import { StyleConstructor } from '../types/style.ts'

/**
 * Adds metadata to an {@link IStyle} class
 */
export function StyleDetails(params: {
  name: string
  displayName: string
  description: string
}) {
  return function <This extends StyleConstructor>(
    target: This,
    context: ClassDecoratorContext<This>,
  ): This {
    if (typeof context.metadata !== 'undefined') {
      Object.assign(context.metadata, params)
    }

    return target
  }
}
