import { StyleDetails } from '../decorators/style-details.ts'
import { ConversationConfig } from '../types/conversation-config.ts'
import { Conversation } from '../types/conversation.ts'
import { Settings } from '../types/settings.ts'
import { BaseStyle } from './base-style.tsx'

@StyleDetails({
  name: 'basic',
  displayName: 'Basic',
  description: 'A minimal style.',
})
export class BasicStyle extends BaseStyle<Settings, Settings> {
  constructor(
    conversation: Conversation,
    conversationConfig: ConversationConfig,
  ) {
    super(conversation, conversationConfig, Settings, Settings)
  }
}
