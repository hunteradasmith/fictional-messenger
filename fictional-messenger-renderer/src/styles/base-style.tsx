import '@tsmetadata/polyfill'
import React, { JSX, type PropsWithChildren } from 'react'
import ReactDOMServer from 'react-dom/server'
import { type MessageCollection } from '../types/message-collection.ts'
import { SettingsConstructor, type Settings } from '../types/settings.ts'
import { type Style } from '../types/style.ts'
import { type Conversation } from '../types/conversation.ts'
import { UserWithSettings } from '../types/user-with-settings.ts'
import { ConversationConfigError } from '../errors/conversation-config-error.ts'
import { StyleInfo } from '../types/style-info.ts'
import { ConversationConfig } from '../types/conversation-config.ts'
import { SettingInfo } from '../types/setting-info.ts'
import { joinClassNames } from '../utils/css-utils.ts'

export type RenderMessagesContainerProps = PropsWithChildren<{
  /** Additional class names that will be added to this component. */
  additionalClassNames?: string
}>

export type RenderMessagesCollectionProps<TUserSettings extends Settings> =
  PropsWithChildren<{
    /** {@link MessageCollection} that the component will render */
    messageCollection: MessageCollection

    /** {@link User} that sent the message collection */
    user: UserWithSettings<TUserSettings>

    /** Additional class names that will be added to this component. */
    additionalClassNames?: string
  }>

export interface RenderSystemMessageProps {
  /** Message that the component will render */
  message: string

  /** Additional class names that will be added to this component. */
  additionalClassNames?: string
}

export interface RenderMessageProps<TUserSettings extends Settings> {
  /** Message that the component will render */
  message: string

  /** {@link User} that sent the message */
  user: UserWithSettings<TUserSettings>

  /** Additional class names that will be added to this component. */
  additionalClassNames?: string
}

export interface RenderUserNameProps<TUserSettings extends Settings> {
  /** {@link User} whose display name will be rendered */
  user: UserWithSettings<TUserSettings>

  /** Additional class names that will be added to this component. */
  additionalClassNames?: string
}

/**
 * Base class that describes a conversation style. Generated HTML may be modified by subclasses.
 */
export class BaseStyle<
  TSettings extends Settings,
  TUserSettings extends Settings,
> implements Style
{
  private _userSettingInfos: SettingInfo[]

  protected settings: TSettings
  protected messageCollections: MessageCollection[]
  protected usersWithSettings: UserWithSettings<TUserSettings>[]

  public constructor(
    conversation: Conversation,
    conversationConfig: ConversationConfig,
    settingsClass: SettingsConstructor<TSettings>,
    userSettingsClass: SettingsConstructor<TUserSettings>,
  ) {
    this.messageCollections = conversation.messageCollections
    this.settings = new settingsClass(conversationConfig.settingValues)
    this.usersWithSettings = conversationConfig.users.map(
      (user) => new UserWithSettings(user, userSettingsClass),
    )

    this._userSettingInfos = new userSettingsClass({}).settingInfos

    this.bindComponentMethods()
  }

  public get styleInfo(): StyleInfo {
    const metadata = this.constructor[Symbol.metadata]

    if (typeof metadata === 'undefined' || metadata === null) {
      throw new Error('Got null or undefined metadata when building style info')
    }

    return {
      name: metadata['name'] as string,
      displayName: metadata['displayName'] as string,
      description: metadata['description'] as string,
      settings: this.settings.settingInfos,
      userSettings: this._userSettingInfos,
    }
  }

  public renderHtml(): string {
    return ReactDOMServer.renderToString(this.renderConversation())
  }

  /**
   * React functional component that creates the markup for a conversation.
   *
   * @remarks
   * This method should only be overridden when making drastic changes to the generated HTML.
   * In most cases, overriding the other components will be simpler.
   */
  protected renderConversation(): JSX.Element {
    return (
      <blockquote
        className={
          'fm-conversation fm-conversation-style-' + this.styleInfo.name
        }
      >
        <this.renderMessagesContainer>
          {this.messageCollections.map((mc, i) => {
            if (mc.username === 'system') {
              return mc.messages.map((msg, i) => (
                <this.renderSystemMessage message={msg} key={i} />
              ))
            } else {
              const user = this.getUser(mc.username)

              return (
                <this.renderMessageCollection
                  messageCollection={mc}
                  user={user}
                  key={i}
                >
                  <this.renderUserName user={user} />
                  {mc.messages.map((msg, i) => (
                    <this.renderMessage message={msg} user={user} key={i} />
                  ))}
                </this.renderMessageCollection>
              )
            }
          })}
        </this.renderMessagesContainer>
      </blockquote>
    )
  }

  /**
   * React functional component that renders an element which contains the conversation
   */
  protected renderMessagesContainer({
    additionalClassNames,
    children,
  }: RenderMessagesContainerProps): JSX.Element {
    return (
      <div
        className={joinClassNames('messages-container', additionalClassNames)}
      >
        {children}
      </div>
    )
  }

  /**
   * React functional component that renders a {@link MessageCollection}
   */
  protected renderMessageCollection({
    additionalClassNames,
    children,
  }: RenderMessagesCollectionProps<TUserSettings>): JSX.Element {
    /**
     * Trailing <br /> adds spacing between message collections when rendered without CSS
     */
    return (
      <div
        className={joinClassNames('message-collection', additionalClassNames)}
      >
        {children}
        <br />
      </div>
    )
  }

  /**
   * React functional component that renders a message
   *
   * @remarks
   * Styles that override this method should be sure to use the dangerouslySetInnerHTML property if
   * HTML in messages should be rendered as part of the result instead of escaped.
   */
  protected renderMessage({
    additionalClassNames,
    message,
  }: RenderMessageProps<TUserSettings>): JSX.Element {
    return (
      <p
        className={joinClassNames('message', additionalClassNames)}
        dangerouslySetInnerHTML={{ __html: message }}
      ></p>
    )
  }

  /**
   * React functional component that renders a system message (e.g. a notice of a
   * user joining or leaving the conversation, or changing their username)
   * @remarks
   * Styles that override this method should be sure to use the dangerouslySetInnerHTML property if
   * HTML in messages should be rendered as part of the result instead of escaped.
   */
  protected renderSystemMessage({
    additionalClassNames,
    message,
  }: RenderSystemMessageProps): JSX.Element {
    return (
      <p className={joinClassNames('system-message', additionalClassNames)}>
        <em dangerouslySetInnerHTML={{ __html: message }}></em>
      </p>
    )
  }

  /**
   * React functional component that renders a {@link User | User's} display name
   */
  protected renderUserName({
    additionalClassNames,
    user,
  }: RenderUserNameProps<TUserSettings>): JSX.Element {
    return (
      <p className={joinClassNames('user-name', additionalClassNames)}>
        <strong>{user.displayName}</strong>
      </p>
    )
  }

  /**
   * Gets a {@link UserWithSettings} by username.
   */
  private getUser(username: string): UserWithSettings<TUserSettings> {
    const user = this.usersWithSettings.find((u) => u.name === username)

    if (typeof user === 'undefined') {
      throw new ConversationConfigError(
        `Conversation config contains no user definition for username "${username}"`,
      )
    }

    return user
  }

  /**
   * Binds all class methods that are React components to this instance's scope.
   * (Otherwise they'll lose association to their instance during render.)
   */
  private bindComponentMethods(): void {
    ;[
      this.renderConversation,
      this.renderMessagesContainer,
      this.renderMessageCollection,
      this.renderMessage,
      this.renderSystemMessage,
      this.renderUserName,
    ].forEach((f) => f.bind(this))
  }
}
