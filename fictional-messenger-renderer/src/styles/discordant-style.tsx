import React, { JSX } from 'react'
import {
  BaseStyle,
  type RenderUserNameProps,
  type RenderMessagesCollectionProps,
} from './base-style.tsx'
import { classesForColor } from '../utils/color-utils.ts'
import { Settings } from '../types/settings.ts'
import { Setting } from '../decorators/setting.ts'
import { HslColor } from '../types/color.ts'
import { StyleDetails } from '../decorators/style-details.ts'
import { ConversationConfig } from '../types/conversation-config.ts'
import { Conversation } from '../types/conversation.ts'

class DiscordantSettings extends Settings {}

class DiscordantUserSettings extends Settings {
  @Setting({
    displayName: 'Profile Image URL',
    description:
      "URL to an image that will be used as this user's profile picture.",
  })
  public accessor profileImageUrl = ''

  @Setting({
    displayName: 'Profile Image Color',
    description:
      "Color to use in place of the profile image if it isn't provided.",
  })
  public accessor profileImageColor: HslColor = { hue: 205, sat: 80, lit: 90 }

  @Setting({
    displayName: 'Name Color',
    description: "Color that will be used for displaying this user's name.",
  })
  public accessor nameColor: HslColor = { hue: 0, sat: 0, lit: 100 }
}

@StyleDetails({
  name: 'discordant',
  displayName: 'Discordant',
  description: 'A style similar to the Discord messaging platform.',
})
export class DiscordantStyle extends BaseStyle<
  DiscordantSettings,
  DiscordantUserSettings
> {
  constructor(
    conversation: Conversation,
    conversationConfig: ConversationConfig,
  ) {
    super(
      conversation,
      conversationConfig,
      DiscordantSettings,
      DiscordantUserSettings,
    )
  }

  protected override renderMessageCollection = (
    props: RenderMessagesCollectionProps<DiscordantUserSettings>,
  ): JSX.Element => {
    const imgUrl =
      props.user.settings.profileImageUrl === ''
        ? undefined
        : props.user.settings.profileImageUrl
    let imgColorClasses = ''

    const imgColor = props.user.settings.profileImageColor
    imgColorClasses = classesForColor('bg', imgColor)

    const SuperMC = super.renderMessageCollection

    // <img> width set for when CSS isn't available
    return (
      <div className="message-collection-outer">
        {imgUrl ? (
          <img
            src={imgUrl}
            className={'profile-image'}
            width={40}
            height={40}
            alt=""
          />
        ) : (
          <div
            className={'profile-image ' + imgColorClasses}
            aria-hidden="true"
          />
        )}
        <SuperMC {...props} />
      </div>
    )
  }

  protected override renderUserName = (
    props: RenderUserNameProps<DiscordantUserSettings>,
  ): JSX.Element => {
    const nameColor = props.user.settings.nameColor
    const nameColorClasses = classesForColor('fg', nameColor)

    return super.renderUserName({
      ...props,
      additionalClassNames: nameColorClasses,
    })
  }
}
