import { type Conversation } from '../types/conversation.ts'
import { type StyleConstructor } from '../types/style.ts'
import { type StyleInfo } from '../types/style-info.ts'
import { ConversationConfig } from '../types/conversation-config.ts'
import { BasicStyle } from './basic-style.tsx'
import { SmsStyle } from './sms-style.tsx'
import { DiscordantStyle } from './discordant-style.tsx'
import {
  conversationCss,
  conversationCssLastModified,
} from '../generated/conversation-css.ts'

const stylesToRegister: StyleConstructor[] = [
  BasicStyle,
  SmsStyle,
  DiscordantStyle,
]

/** Map of {@link Style} names to {@link StyleInfo} instances containing details about respective Styles */
const styleInfoByName: Record<string, StyleInfo> = {}

/** Map of {@link Style} names to constructor methods for respective Style */
const styleConstructorByName: Record<string, StyleConstructor> = {}

const emptyConversation: Conversation = {
  messageCollections: [],
  configName: '',
}

const emptyConversationConfig: ConversationConfig = {
  style: '',
  settingValues: {},
  users: [],
}

for (const styleConstructor of stylesToRegister) {
  const emptyInstance = new styleConstructor(
    emptyConversation,
    emptyConversationConfig,
  )
  const name = emptyInstance.styleInfo.name

  styleInfoByName[name] = emptyInstance.styleInfo
  styleConstructorByName[name] = styleConstructor
}

export const styles = {
  /**
   * Object mapping style names to {@link StyleInfo} instances.
   */
  get infoByName(): Record<string, StyleInfo> {
    return { ...styleInfoByName }
  },

  /**
   * Object mapping style names to style constructor methods.
   */
  get constructorByName(): Record<string, StyleConstructor> {
    return { ...styleConstructorByName }
  },

  /**
   * String containing the generated CSS for Fictional Messenger's styles.
   */
  get conversationCss(): string {
    return conversationCss
  },

  /**
   * {@link Date} indicating the last time the generated CSS was modified.
   */
  get conversationCssLastModified(): Date {
    return conversationCssLastModified
  },
}
