import { sampleConfig, sampleConversation } from '../utils/test-utils.ts'
import { styles } from './index.ts'

for (const styleConstructor of Object.values(styles.constructorByName)) {
  describe(styleConstructor, () => {
    it('renders html that matches its snapshot', () => {
      const style = new styleConstructor(sampleConversation, sampleConfig)
      expect(style.renderHtml()).toMatchSnapshot()
    })
  })
}
