import { JSX } from 'react'
import {
  BaseStyle,
  type RenderMessagesContainerProps,
  type RenderMessageProps,
  type RenderMessagesCollectionProps,
} from './base-style.tsx'
import { classesForColor } from '../utils/color-utils.ts'
import { StyleDetails } from '../decorators/style-details.ts'
import { ConversationConfig } from '../types/conversation-config.ts'
import { Conversation } from '../types/conversation.ts'
import { Settings } from '../types/settings.ts'
import { Setting } from '../decorators/setting.ts'
import { HslColor } from '../types/color.ts'
import { joinClassNames } from '../utils/css-utils.ts'

class SmsSettings extends Settings {
  @Setting({
    displayName: 'Text Color',
    description: 'Text color of the conversation.',
  })
  public accessor textColor: HslColor = { hue: 0, sat: 0, lit: 10 }

  @Setting({
    displayName: 'Background Color',
    description: 'Background color of the conversation.',
  })
  public accessor bgColor: HslColor = { hue: 0, sat: 0, lit: 100 }

  @Setting({
    displayName: 'Message Background Color',
    description:
      'Text color of messages by non-primary users (e.g. not the owner of the device displaying the conversation.)',
  })
  public accessor messageBgColor: HslColor = { hue: 0, sat: 0, lit: 90 }

  @Setting({
    displayName: 'Primary Message Background Color',
    description:
      'Text color of messages by primary users (e.g. the owner of the device displaying the conversation.)',
  })
  public accessor primaryMessageBgColor: HslColor = {
    hue: 120,
    sat: 100,
    lit: 90,
  }
}

class SmsUserSettings extends Settings {
  @Setting({
    displayName: 'Primary?',
    description:
      'Specifies which user is the primary user (i.e. the user on whose device the conversation is being displayed.)',
  })
  public accessor primary = false
}

@StyleDetails({
  name: 'sms',
  displayName: 'SMS',
  description: 'An SMS-like style.',
})
export class SmsStyle extends BaseStyle<SmsSettings, SmsUserSettings> {
  constructor(
    conversation: Conversation,
    conversationConfig: ConversationConfig,
  ) {
    super(conversation, conversationConfig, SmsSettings, SmsUserSettings)
  }

  protected override renderMessagesContainer = (
    props: RenderMessagesContainerProps,
  ): JSX.Element => {
    const bgColorClasses = classesForColor('bg', this.settings.bgColor)
    return super.renderMessagesContainer({
      ...props,
      additionalClassNames: bgColorClasses,
    })
  }

  protected override renderMessageCollection = (
    props: RenderMessagesCollectionProps<SmsUserSettings>,
  ): JSX.Element => {
    const primaryClass = props.user.settings.primary ? 'primary' : ''
    const textColorClasses = classesForColor('fg', this.settings.textColor)

    return super.renderMessageCollection({
      ...props,
      additionalClassNames: joinClassNames(primaryClass, textColorClasses),
    })
  }

  protected override renderMessage = (
    props: RenderMessageProps<SmsUserSettings>,
  ): JSX.Element => {
    const isPrimary = props.user.settings.primary
    const bgColorClasses = classesForColor(
      'bg',
      isPrimary
        ? this.settings.primaryMessageBgColor
        : this.settings.messageBgColor,
    )

    return super.renderMessage({
      ...props,
      additionalClassNames: bgColorClasses,
    })
  }
}
