export interface HslColor {
  /**
   * Hue component of this color. Must be between 0 and 359.
   */
  hue: number

  /**
   * Saturation component of this color. Must be between 0 and 100.
   */
  sat: number

  /**
   * Lightness component of this color. Must be between 0 and 100.
   */
  lit: number
}
