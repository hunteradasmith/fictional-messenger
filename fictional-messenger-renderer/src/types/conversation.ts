import { type MessageCollection } from './message-collection.ts'

/**
 * Describes a conversation.
 */
export interface Conversation {
  /** The messages that make up this conversation */
  messageCollections: MessageCollection[]

  /** This conversation's optional configuration name */
  configName?: string
}
