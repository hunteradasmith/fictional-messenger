/**
 * Valid types of settings represented by a {@link SettingInfo} instance.
 */
export enum SettingType {
  String = 'string',
  Number = 'number',
  Boolean = 'boolean',
  HslColor = 'hslColor',
}
