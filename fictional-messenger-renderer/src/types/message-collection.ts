/**
 * Contains one or more sequential message strings from a particular {@link User}.
 */
export interface MessageCollection {
  /** A sequence of messages. */
  messages: string[]

  /** The username of the {@link User} which sent the messages */
  username: string
}
