import { Settings, SettingsConstructor } from './settings.ts'
import { User } from './user.ts'

export class UserWithSettings<TSettings extends Settings> extends User {
  public settings: TSettings

  constructor(user: User, settingsConstructor: SettingsConstructor<TSettings>) {
    super(user.name, user.displayName, user.settingValues)
    this.settings = new settingsConstructor(this.settingValues)
  }
}
