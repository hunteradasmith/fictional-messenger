import { type User } from './user.ts'

/**
 * Describes the configuration of a conversation, including the name of the {@link Style}
 * in which the conversation will be rendered, that style's settings, and the
 * {@link User | Users} in the conversation.
 *
 * @privateRemarks
 * Uses the {@link https://github.com/typestack/class-transformer | class-transformer} module
 * for serialization and deserialization.
 */
export interface ConversationConfig {
  /** Name of the {@link Style} in which the conversation will be rendered. */
  style: string

  /** Settings for the {@link Style} in which the conversation will be rendered. */
  settingValues: Record<string, any>

  /** {@link User | Users} in the conversation. */
  users: User[]
}
