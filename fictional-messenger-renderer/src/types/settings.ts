import '@tsmetadata/polyfill'
import { isValidColor, nearestColor } from '../utils/color-utils.ts'
import { SettingInfo } from './setting-info.ts'
import { SettingType } from './setting-type.ts'

/**
 * Maps {@link SettingType | SettingTypes} to functions that validate values with that type
 */
export const SettingTypeValidators: Record<
  SettingType,
  (value: any, settingInfo: SettingInfo) => boolean
> = {
  [SettingType.String]: (value) => typeof value === 'string',
  [SettingType.Number]: (value) => typeof value === 'number' && isFinite(value),
  [SettingType.Boolean]: (value) => typeof value === 'boolean',
  [SettingType.HslColor]: (value) => isValidColor(value),
}

/**
 * Type of a class constructor method that returns an instance of a {@link Settings} subclass.
 */
export type SettingsConstructor<TSettings> = new (
  settingValues: Record<string, any>,
) => TSettings

/**
 * Class representing a {@link Style | Style's} or {@link User | User's} settings.
 *
 * @privateRemarks
 * Works with the {@link Setting} decorator in order to register {@link SettingInfo | SettingInfos}
 * that contain metadata about each setting on subclasses of this class.
 */
export class Settings {
  private _settingInfos: SettingInfo[] = []
  private _settingValues: Record<string, any>

  /**
   * Utility methods for working with particular setting types
   */
  public static Utils = {
    /**
     * Returns {@link HslColor} that is the nearest valid color to the
     * provided, possibly-invalid {@link HslColor}.
     */
    nearestColor,
  }

  /**
   * Gets an array of {@link SettingInfo} describing this instance's available settings.
   */
  public get settingInfos(): SettingInfo[] {
    return [...this._settingInfos]
  }

  constructor(settingValues: Record<string, any>) {
    this._settingValues = settingValues ?? {}
  }

  /**
   * Returns the provided value, or if the value is null or undefined,
   * returns the default value from the provided {@link SettingInfo}.
   */
  public static getSettingValueOrDefault(
    settingInfo: SettingInfo,
    value: any,
  ): any {
    if (typeof value === 'undefined' || value === null) {
      return settingInfo.defaultValue
    }

    return value
  }

  /**
   * Returns a boolean indicating whether the provided value is valid for
   * the setting type specified in the provided {@link SettingInfo}.
   */
  public static isValueValid(settingInfo: SettingInfo, value: any): boolean {
    const validate = SettingTypeValidators[settingInfo.type]

    if (typeof validate === 'undefined') {
      throw new TypeError(
        `Could not find a validator for setting type "${settingInfo.type}"`,
      )
    }

    return validate(value, settingInfo)
  }

  /**
   * If the provided value is not valid for the type specified in the
   * provided {@link SettingInfo}, throws a {@link TypeError} exception.
   */
  public static assertValueValid(settingInfo: SettingInfo, value: any): void {
    if (!Settings.isValueValid(settingInfo, value)) {
      throw new TypeError(
        `Value of setting ${settingInfo.name} is not valid for type ${settingInfo.type}. Value: ${JSON.stringify(value)}`,
      )
    }
  }

  /**
   * Registers a {@link SettingInfo} describing a setting accessor on a subclass,
   * then returns a value for that setting.
   */
  protected registerSettingInfo(settingInfo: SettingInfo): any {
    this._settingInfos.push(settingInfo)
    const valueOrDefault = Settings.getSettingValueOrDefault(
      settingInfo,
      this._settingValues[settingInfo.name],
    )
    Settings.assertValueValid(settingInfo, valueOrDefault)
    return valueOrDefault
  }
}
