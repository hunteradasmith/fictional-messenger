/**
 * Describes a user in a conversation
 */
export class User {
  /**
   * Internal name of the {@link User}. (This is the name used in conversation text but is not
   * included in the rendered result.
   */
  public name: string

  /** Display name of the {@link User}. */
  public displayName: string

  /** The {@link User | User's} settings. */
  public settingValues: Record<string, any>

  constructor(
    name: string,
    displayName: string,
    settingValues: Record<string, any> = {},
  ) {
    this.name = name
    this.displayName = displayName
    this.settingValues = settingValues
  }
}
