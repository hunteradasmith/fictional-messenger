import { type SettingType } from './setting-type.ts'

/**
 * Describes a setting of a {@link Style} or {@link User}.
 */
export interface SettingInfo {
  /** Internal name of the setting. */
  name: string

  /** Display name of the setting. */
  displayName: string

  /** A brief description of the setting. */
  description: string

  /** Setting's default value. May be null. */
  defaultValue: any

  /** Type of the setting. */
  type: SettingType
}
