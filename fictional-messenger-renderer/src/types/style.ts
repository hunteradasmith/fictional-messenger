import { ConversationConfig } from './conversation-config.ts'
import { type Conversation } from './conversation.ts'
import { StyleInfo } from './style-info.ts'

/**
 * Describes a constructor function that returns a new {@link Style} instance
 */
export type StyleConstructor = new (
  conversation: Conversation,
  conversationConfig: ConversationConfig,
) => Style

/**
 * Describes a conversation style, with methods for rendering HTML and CSS.
 */
export interface Style {
  get styleInfo(): StyleInfo

  /**
   * Renders this {@link Style} to HTML.
   */
  renderHtml: () => string
}
