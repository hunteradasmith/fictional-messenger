import { type SettingInfo } from './setting-info.ts'

/**
 * Describes a {@link Style}
 *
 * @remarks
 * Instances of this interface are intended for use by code that need
 * to discover metadata about the available styles, for example the
 * fictional-messenger web app itself.
 */
export interface StyleInfo {
  /** Internal name of the {@link Style}. */
  name: string

  /** Display name of the {@link Style}. */
  displayName: string

  /** Brief description of the {@link Style}. */
  description: string

  /** Information about the {@link Style | Style's} settings. */
  settings: SettingInfo[]

  /** Information about the {@link Style | Style's} {@link User} settings. */
  userSettings: SettingInfo[]
}
