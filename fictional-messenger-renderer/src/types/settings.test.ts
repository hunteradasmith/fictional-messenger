import { Setting } from '../decorators/setting.ts'
import { type HslColor as HslColor } from './color.ts'
import { SettingType } from './setting-type.ts'
import { Settings } from './settings.ts'

const minColor: HslColor = { hue: 0, sat: 0, lit: 0 }

class TestSettings extends Settings {
  @Setting({
    displayName: 'test-string',
    description: '',
  })
  public accessor testString = 'default-value'

  @Setting({
    displayName: 'test-number',
    description: '',
  })
  public accessor testNumber = 12345

  @Setting({
    displayName: 'test-boolean',
    description: '',
  })
  public accessor testBoolean = true

  @Setting({
    displayName: 'test-color',
    description: '',
  })
  public accessor testColor: HslColor = minColor
}

describe(Settings, () => {
  const testSettings = new TestSettings({})
  const settingInfoFor = (name: keyof TestSettings) => {
    const settingInfo = testSettings.settingInfos.find((i) => i.name === name)

    if (typeof settingInfo === 'undefined') {
      throw Error('No setting info for ' + name)
    }

    return settingInfo
  }

  describe(Settings.isValueValid, () => {
    const validateAll = (objs: any[], name: keyof TestSettings): boolean[] =>
      objs.map((o) => Settings.isValueValid(settingInfoFor(name), o))

    const allValid = (objs: any[], name: keyof TestSettings): boolean =>
      validateAll(objs, name).filter((v) => !v).length === 0

    const allInvalid = (objs: any[], name: keyof TestSettings): boolean =>
      validateAll(objs, name).filter((v) => v).length === 0

    describe(SettingType.String, () => {
      it('returns true for valid strings', () => {
        const strings = ['', 'string']
        expect(allValid(strings, 'testString')).toBe(true)
      })

      it('returns false for non-string values', () => {
        const notStrings = [123, false, null, undefined, [], {}]
        expect(allInvalid(notStrings, 'testString')).toBe(true)
      })
    })

    describe(SettingType.Number, () => {
      it('returns true for valid numbers', () => {
        const numbers = [0, 12345, -123, 0.000001]
        expect(allValid(numbers, 'testNumber')).toBe(true)
      })

      it('returns false for non-number values', () => {
        const notNumbers = ['123', false, null, undefined, [], {}]
        expect(allInvalid(notNumbers, 'testNumber')).toBe(true)
      })
    })

    describe(SettingType.Boolean, () => {
      it('returns true for valid booleans', () => {
        const bools = [true, false]
        expect(allValid(bools, 'testBoolean')).toBe(true)
      })

      it('returns false for non-boolean values', () => {
        const notBools = [123, 'abc', null, undefined, [], {}]
        expect(allInvalid(notBools, 'testBoolean')).toBe(true)
      })
    })

    describe(SettingType.HslColor, () => {
      it('returns true for valid colors', () => {
        const colors: HslColor[] = [
          minColor,
          { hue: 125, sat: 20, lit: 90 },
          { hue: 355, sat: 100, lit: 100 },
        ]

        expect(allValid(colors, 'testColor')).toBe(true)
      })

      it('returns false for invalid colors', () => {
        const notColors = [
          { hue: 1, sat: 0, lit: 0 }, // hue not multiple of 5
          { hue: 0, sat: 0, lit: 110 }, // lit past maximum of 100
          { hue: 0, sat: -69, lit: 0 }, // sat below minimum of 0
        ]
        expect(allInvalid(notColors, 'testColor')).toBe(true)
      })

      it('returns false for non-string values', () => {
        const notColors = [123, false, null, undefined, [], {}]
        expect(allInvalid(notColors, 'testColor')).toBe(true)
      })
    })
  })

  describe(Settings.assertValueValid, () => {
    it('throws an exception on invalid values', () => {
      const settingInfo = settingInfoFor('testString')
      const invalidValue = 1
      expect(() =>
        Settings.assertValueValid(settingInfo, invalidValue),
      ).toThrow(TypeError)
    })

    it('does not throw an exception on valid values', () => {
      const settingInfo = settingInfoFor('testString')
      const validValue = 'abcdef'
      expect(() =>
        Settings.assertValueValid(settingInfo, validValue),
      ).not.toThrow()
    })
  })

  describe(Settings.getSettingValueOrDefault, () => {
    it('returns the provided value if not null or undefined', () => {
      const settingInfo = settingInfoFor('testString')
      const testValue = 'abcdef'
      const result = Settings.getSettingValueOrDefault(settingInfo, testValue)
      expect(result).toBe(testValue)
    })

    it('returns the default value if the provided is null or undefined', () => {
      const settingInfo = settingInfoFor('testString')
      const undefinedResult = Settings.getSettingValueOrDefault(
        settingInfo,
        undefined,
      )
      expect(undefinedResult).toBe(settingInfo.defaultValue)

      const nullResult = Settings.getSettingValueOrDefault(settingInfo, null)
      expect(nullResult).toBe(settingInfo.defaultValue)
    })
  })
})
