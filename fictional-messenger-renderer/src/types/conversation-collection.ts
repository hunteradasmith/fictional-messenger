import { type ConversationConfig } from './conversation-config.ts'
import { type Conversation } from './conversation.ts'

/**
 * Describes a collection of conversations.
 */
export interface ConversationCollection {
  /** The {@link Conversation | Conversations} in this collection */
  conversations: Conversation[]

  /** The {@link ConversationConfigs} in this collection's conversations, by name */
  configsByName: Record<string, ConversationConfig>

  /**
   * Text sections that will be inserted before, between, and after each
   * {@link Conversation} when this ConversationCollection is rendered.
   *
   * @remarks
   * These sections will be interleaved with the rendered {@link Conversation | Conversations},
   * with the first text conversation coming before the first Conversation. Absent text sections
   * will be replaced with empty strings.
   */
  textSections: string[]
}
