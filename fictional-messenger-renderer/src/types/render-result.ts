/**
 * The result of rendering a {@link Conversation} or {@link ConversationCollection}
 */
export interface RenderResult {
  /**
   * The rendered HTML.
   */
  html: string
}
