import lodash from 'lodash'
import { ConversationParseError } from '../errors/conversation-parse-error.ts'
import { type ConversationConfig } from '../types/conversation-config.ts'
import { type Conversation } from '../types/conversation.ts'
import { type MessageCollection } from '../types/message-collection.ts'
import { type User } from '../types/user.ts'

const usernameRegex = /^\s*(\S+):\s*(.*)/

const systemUser: User = {
  name: 'system',
  displayName: 'system',
  settingValues: {},
}

/**
 * Parses a string containing a chat conversation and generates an array of {@link MessageCollection | MessageCollections}.
 *
 * @remarks
 * Messages start with a {@link User} name and a colon, followed by a message from that user. Subsequent
 * lines that don't start with a user name will be interpreted as a sequential message from that User.
 * Sequential messages from the same User will be grouped together in a single {@link MessageCollection}.
 * Empty lines and lines starting with two forward slashes (`//`) will be ignored.
 *
 * @param conversationText - The conversation to be parsed
 * @param config - Configuration of the conversation to be used when parsing
 * @returns array of {@link MessageCollection | MessageCollections} containing the parsed conversation
 *
 * @throws {@link ConversationParseError}
 * Thrown when there is an error in the conversation syntax. Possible causes include:
 *   - The first non-empty, non-comment line in a conversation does not contain a `:` character separating user name and message
 */
export function parseConversationText(
  conversationText: string,
  baseConfig: ConversationConfig,
): { conversation: Conversation; config: ConversationConfig } {
  const messageCollections: MessageCollection[] = []
  const config = lodash.cloneDeep(baseConfig)

  const enumeratedLines = conversationText
    .split('\n')
    .map((l, i) => ({ line: l.trim(), lineNumber: i }))

  let currentCollection: MessageCollection | undefined

  for (const { line, lineNumber } of enumeratedLines) {
    if (line.length === 0 || line.startsWith('//')) {
      continue
    }

    let username: string
    let message: string

    const matches = line.match(usernameRegex)
    if (matches === null) {
      if (currentCollection === undefined) {
        throw new ConversationParseError(
          "First non-empty, non-comment line doesn't start with a user name and colon character",
          lineNumber,
        )
      }

      username = currentCollection.username
      message = line
    } else {
      username = matches[1]
      message = matches[2]
    }

    if (
      currentCollection === undefined ||
      currentCollection.username !== username
    ) {
      let user =
        username === systemUser.name
          ? systemUser
          : config.users.find((u) => u.name === username)

      if (user === undefined) {
        user = {
          name: username,
          displayName: username,
          settingValues: {},
        }

        config.users.push(user)
      }

      currentCollection = {
        messages: [],
        username,
      }

      messageCollections.push(currentCollection)
    }

    currentCollection.messages.push(message)
  }

  const conversation: Conversation = { messageCollections }
  return { conversation, config }
}
