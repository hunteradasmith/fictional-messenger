import { ConversationParseError } from '../errors/conversation-parse-error.ts'
import {
  otherSampleHtmlSections,
  sampleConfigsByNameJson,
  sampleHtmlConversationCollectionSections,
  sampleHtmlConversationCollectionText,
} from '../utils/test-utils.ts'
import { dissectHtml } from './dissect-html.ts'

const sampleConfigName = 'sample-config'

describe(dissectHtml, () => {
  it('parses correctly-formatted HTML into text, conversation, and config sections', () => {
    const { textSections, conversationSections, configsText } = dissectHtml(
      sampleHtmlConversationCollectionText,
    )

    expect(textSections.length).toBe(3)
    expect(conversationSections.length).toBe(2)

    expect(textSections[0]).toContain(
      sampleHtmlConversationCollectionSections[3],
    )
    expect(textSections[1]).toContain(
      sampleHtmlConversationCollectionSections[5],
    )
    expect(textSections[2]).toContain(
      sampleHtmlConversationCollectionSections[7],
    )

    expect(conversationSections[0].configName).toBe(sampleConfigName)
    expect(conversationSections[1].configName).toBe(sampleConfigName)

    expect(configsText).toContain(sampleConfigsByNameJson)
  })

  it.each(Object.entries(otherSampleHtmlSections))(
    'parses real-world HTML from %s',
    (_, sampleHtml) => {
      const { conversationSections } = dissectHtml(sampleHtml)
      expect(conversationSections.length).toBeGreaterThan(0)
      expect(conversationSections[0].configName).toBe(sampleConfigName)
    },
  )

  it('throws exception when a conversation end marker is missing', () => {
    const missingEndMarker = `
    <p>-- fm: sample-config --</p>
    <p>user1: message</p>
    `

    expect(() => dissectHtml(missingEndMarker)).toThrow(ConversationParseError)
  })
})
