import {
  sampleConfig,
  sampleConversationText,
  sampleHtmlConversationText,
  sampleMarkdownConversationText,
} from '../utils/test-utils.ts'
import { parseConversation } from './parse-conversation.ts'
import { parseConversationText as mockParseConversationText } from './parse-conversation-text.ts'
import { preprocessHtml as mockPreprocessHtml } from './preprocess-html.ts'
import { postprocessMarkdown as mockPostprocessMarkdown } from './postprocess-markdown.ts'

jest.mock('./parse-conversation-text', () => ({
  parseConversationText: jest.fn(() => ({ conversation: null, config: null })),
}))

jest.mock('./preprocess-html', () => ({
  preprocessHtml: jest.fn(),
}))

jest.mock('./postprocess-markdown', () => ({
  postprocessMarkdown: jest.fn(),
}))

describe(parseConversation, () => {
  it('calls parseConversationText to parse plain text conversations', () => {
    parseConversation(sampleConversationText, 'plain', sampleConfig)
    expect(mockParseConversationText).toHaveBeenCalled()
  })

  it('preprocesses html formatted text', () => {
    parseConversation(sampleHtmlConversationText, 'html', sampleConfig)
    expect(mockPreprocessHtml).toHaveBeenCalled()
  })

  it('postprocesses markdown formatted text', () => {
    parseConversation(sampleMarkdownConversationText, 'markdown', sampleConfig)
    expect(mockPostprocessMarkdown).toHaveBeenCalled()
  })
})
