import { type Rule } from 'css'
import parse, { type HTMLElement } from 'node-html-parser'
import {
  parseStyleAttribute,
  removeProperties,
  stringifyStyleAttribute,
} from '../utils/css-utils.ts'

const lineElementsSelector = 'div, p'
const startsWithUsernameRegex = /\s*(\w+:)/

// CSS attributes inserted by certain visual editors (e.g. Google Docs) that cause problems when rendered
const problemStyleProperties = [
  'font-size',
  'font-family',
  'color',
  'background-color',
  'vertical-align',
  'white-space',
  'line-height',
  'margin',
  'margin-top',
  'margin-right',
  'margin-bottom',
  'margin-left',
]

function findStartingUsername(element: HTMLElement): string | null {
  const matches = element.textContent.match(startsWithUsernameRegex)
  if (matches !== null) {
    return matches[1]
  }

  return null
}

function removeProblemStyleProperties(line: string): string {
  const root = parse(line)

  for (const element of root.querySelectorAll('*')) {
    const style = element.getAttribute('style')

    if (typeof style !== 'undefined') {
      const parsed = parseStyleAttribute(style)

      if (parsed !== null) {
        removeProperties(
          parsed.stylesheet?.rules[0] as Rule,
          problemStyleProperties,
        )
        const newStyle = stringifyStyleAttribute(parsed)

        element.setAttribute('style', newStyle)
      }
    }
  }

  return root.innerHTML
}

export function preprocessHtml(text: string): string {
  const lines: string[] = []
  const root = parse(text)

  for (const element of root.querySelectorAll(lineElementsSelector)) {
    const startingUsername = findStartingUsername(element)
    let line = element.innerHTML

    if (startingUsername !== null) {
      // Ensure the line starts with the plain-text username instead of a containing HTML tag
      line = startingUsername + line.replace(startingUsername, '')
    }

    line = removeProblemStyleProperties(line)
    lines.push(line)
  }

  return lines.join('\n')
}
