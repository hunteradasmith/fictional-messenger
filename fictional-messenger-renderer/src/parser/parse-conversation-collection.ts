import { type ConversationConfig } from '../types/conversation-config.ts'
import { type ConversationCollection } from '../types/conversation-collection.ts'
import { parseConversation } from './parse-conversation.ts'
import { dissectHtml } from './dissect-html.ts'
import lodash from 'lodash'
import { parseConversationConfigs } from './parse-conversation-configs.ts'
import { dissectMarkdown } from './dissect-markdown.ts'

export interface DissectedText {
  textSections: string[]
  conversationSections: { text: string; configName: string }[]
  configsText?: string
}

/**
 * Parses text in a specified format and returns a {@link ConversationCollection} instance.
 *
 * @remarks
 * Conversations and conversation configs in the text will be found via start and end markers, which can vary depending
 * on the text format. These markers are:
 *   - html format:
 *     - conversation start: `-- fm: config-name --` (with `config-name` specifying the name of the config for this conversation)
 *     - conversation end: `-- end --`
 *     - config start: `-- fm-configs --`
 *     - config end: `-- end --`
 *   - markdown format:
 *     - conversation start: ```` ```fm: config-name ```` (with `config-name` specifying the name of the config for this conversation)
 *     - conversation end: ```` ``` ````
 *     - config start: ```` ```fm-configs ````
 *     - config end: ```` ``` ````
 *
 * @param text - text to be parsed
 * @param format - format of the text
 * @param baseConfigsByName - optional conversation configs by name to use when parsing the text.
 *  (If not specified, this method will be attempt to locate and parse the config from the text parameter.)
 * @returns a {@link ConversationCollection} parsed from the text
 *
 * @throws {@link ConversationParseError}
 * Thrown when there is an error in a conversation's syntax. Possible causes include:
 *   - The first non-empty, non-comment line does not contain a `:` character separating user name and message
 *   - A conversation start marker was found without a corresponding end marker
 */
export function parseConversationCollection(
  text: string,
  format: 'html' | 'markdown',
  baseConfigsByName: Record<string, ConversationConfig> = {},
): ConversationCollection {
  let dissected: DissectedText

  switch (format) {
    case 'html':
      dissected = dissectHtml(text)
      break
    case 'markdown':
      dissected = dissectMarkdown(text)
      break
  }

  if (
    lodash.isEmpty(baseConfigsByName) &&
    typeof dissected.configsText !== 'undefined' &&
    dissected.configsText.length > 0
  ) {
    baseConfigsByName = parseConversationConfigs(dissected.configsText, 'json')
  }

  const conversationCollection: ConversationCollection = {
    conversations: [],
    configsByName: lodash.cloneDeep(baseConfigsByName) ?? {},
    textSections: dissected.textSections,
  }

  for (const { text, configName } of dissected.conversationSections) {
    let baseConfig = conversationCollection.configsByName[configName]

    if (typeof baseConfig === 'undefined') {
      baseConfig = conversationCollection.configsByName[configName] = {
        style: 'basic',
        settingValues: {},
        users: [],
      }
    }

    const { conversation, config } = parseConversation(text, format, baseConfig)
    conversation.configName = configName

    conversationCollection.conversations.push(conversation)
    conversationCollection.configsByName[configName] = config
  }

  return conversationCollection
}
