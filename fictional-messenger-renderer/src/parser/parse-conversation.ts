import { type Conversation } from '../types/conversation.ts'
import { type ConversationConfig } from '../types/conversation-config.ts'
import { parseConversationText } from './parse-conversation-text.ts'
import { preprocessHtml } from './preprocess-html.ts'
import { postprocessMarkdown } from './postprocess-markdown.ts'

/**
 * Parses text in a specified format and returns a {@link Conversation} instance.
 *
 * @remarks
 * Messages start with a {@link User} name and a colon, followed by a message from that user. Subsequent
 * lines that don't start with a user name will be interpreted as a following message from that User.
 * Empty lines and lines starting with two forward slashes (`//`) will be ignored.
 *
 * @param text - text to be parsed
 * @param format - format of the text
 * @param baseConfig - conversation config to use when parsing the text
 * @returns a {@link Conversation} parsed from the text
 *
 * @throws {@link ConversationParseError}
 * Thrown when there is an error in the conversation syntax. Possible causes include:
 *   - The first non-empty, non-comment line does not contain a `:` character separating user name and message
 */
export function parseConversation(
  text: string,
  format: 'plain' | 'html' | 'markdown',
  baseConfig: ConversationConfig,
): { conversation: Conversation; config: ConversationConfig } {
  let processedText: string

  switch (format) {
    case 'html':
      processedText = preprocessHtml(text)
      break
    default:
      processedText = text
  }

  const ret = parseConversationText(processedText, baseConfig)
  const { conversation, config } = ret

  switch (format) {
    case 'markdown':
      postprocessMarkdown(conversation)
      break
    default:
  }

  return { conversation, config }
}
