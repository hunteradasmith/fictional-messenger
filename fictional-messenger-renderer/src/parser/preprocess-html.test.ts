import { sampleHtmlConversationText } from '../utils/test-utils.ts'
import { preprocessHtml } from './preprocess-html.ts'

describe(preprocessHtml, () => {
  it('removes enclosing tags from message lines', () => {
    const processed = preprocessHtml(sampleHtmlConversationText)
    const expected =
      'user1:<span> Test message one</span>\nuser2: <em>Test message two</em>\nuser2:<span></span> <em>Test message three</em>'

    expect(processed).toBe(expected)
  })

  it('removes tags surrounding usernames at message start while preserving message html', () => {
    const taggedUsername =
      '<p><span>user1:</span> message <em>with html</em></p>'
    const expected = 'user1:<span></span> message <em>with html</em>'

    const processed = preprocessHtml(taggedUsername)
    expect(processed).toBe(expected)
  })

  it('removes problematic style attribute properties', () => {
    const problemStyle =
      '<p>user1: <span style="font-size:1px;font-weight:italic;">text</span></p>'
    const expected = 'user1: <span style="font-weight:italic;">text</span>'

    const processed = preprocessHtml(problemStyle)
    expect(processed).toBe(expected)
  })
})
