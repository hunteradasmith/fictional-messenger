import {
  sampleConfigsByNameJson,
  sampleMarkdownConversationCollectionText,
  sampleMarkdownConversationCollectionSections,
} from '../utils/test-utils.ts'
import { dissectMarkdown } from './dissect-markdown.ts'

const sampleConfigName = 'sample-config'

// TODO more comprehensive tests
describe(dissectMarkdown, () => {
  it('parses Markdown into text, conversation, and config sections', () => {
    const { textSections, conversationSections, configsText } = dissectMarkdown(
      sampleMarkdownConversationCollectionText,
    )

    expect(textSections.length).toBe(3)
    expect(conversationSections.length).toBe(2)

    expect(textSections[0]).toContain(
      sampleMarkdownConversationCollectionSections[3],
    )
    expect(textSections[1]).toContain(
      sampleMarkdownConversationCollectionSections[7],
    )
    expect(textSections[2]).toContain(
      sampleMarkdownConversationCollectionSections[11],
    )

    expect(conversationSections[0].configName).toBe(sampleConfigName)
    expect(conversationSections[1].configName).toBe(sampleConfigName)

    expect(configsText).toContain(sampleConfigsByNameJson)
  })
})
