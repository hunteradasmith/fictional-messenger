import { parseConversationText } from './parse-conversation-text.ts'
import {
  sampleConfig,
  sampleConversationText,
  sampleConversationMessageCollections,
} from '../utils/test-utils.ts'
import { ConversationParseError } from '../errors/conversation-parse-error.ts'

describe(parseConversationText, () => {
  it('parses conversations into collections with users', () => {
    const {
      conversation: { messageCollections },
    } = parseConversationText(sampleConversationText, sampleConfig)

    expect(messageCollections).toMatchObject(
      sampleConversationMessageCollections,
    )
  })

  it('parses conversations including system user without system in users', () => {
    const systemConversation = 'system: system message'
    const {
      conversation: { messageCollections },
    } = parseConversationText(systemConversation, sampleConfig)

    expect(messageCollections[0].username).toBe('system')
  })

  it('ignores lines starting with two forward slashes', () => {
    const commented = '// ignored line'
    const {
      conversation: { messageCollections },
    } = parseConversationText(commented, sampleConfig)

    expect(messageCollections.length).toBe(0)
  })

  it("adds sequential lines without user name and colon to last user's collection", () => {
    const sequential = `
    user1: first message from this user
    second message
    third message
    `

    const {
      conversation: { messageCollections },
    } = parseConversationText(sequential, sampleConfig)
    expect(messageCollections[0].messages.length).toBe(3)
  })

  it('throws error on first line without user name and colon separator', () => {
    const invalid = `
    invalid first line
    user1: valid line
    `

    expect(() => parseConversationText(invalid, sampleConfig)).toThrow(
      ConversationParseError,
    )
  })

  it("doesn't incorrectly parse usernames from colons after non-usernames", () => {
    const text = `
    user1: a message
    a message: with a colon, from the same user
    `
    const { conversation } = parseConversationText(text, sampleConfig)
    expect(conversation.messageCollections).toHaveLength(1)
    expect(conversation.messageCollections[0].messages).toHaveLength(2)
  })

  it('creates new users for user names that do not exist in provide array', () => {
    const inexistentUsername = 'non-existing-username'
    const inexistentConversation = `${inexistentUsername}: a message`
    const { conversation, config } = parseConversationText(
      inexistentConversation,
      sampleConfig,
    )

    expect(conversation.messageCollections[0].username).toBe(inexistentUsername)

    expect(
      sampleConfig.users.find((u) => u.name === inexistentUsername),
    ).not.toBeDefined()
    expect(
      config.users.find((u) => u.name === inexistentUsername),
    ).toBeDefined()
  })
})
