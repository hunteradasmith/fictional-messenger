import { type ConversationConfig } from '../types/conversation-config.ts'

export function parseConversationConfigs(
  text: string,
  format: 'json',
): Record<string, ConversationConfig> {
  // TODO validation of parsed config
  switch (format) {
    case 'json':
      return JSON.parse(text) as Record<string, ConversationConfig>
  }
}
