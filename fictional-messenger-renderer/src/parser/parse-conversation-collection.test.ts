/* eslint @typescript-eslint/no-require-imports: 0 */ // Require imports used to fetch sample values due to Jest scoping

import {
  sampleConfig,
  sampleConfigName,
  sampleConfigsByName,
  sampleHtmlConversationCollectionText,
} from '../utils/test-utils.ts'
import { dissectHtml as mockDissectHtml } from './dissect-html.ts'
import { parseConversation as mockParseConversation } from './parse-conversation.ts'
import { parseConversationCollection } from './parse-conversation-collection.ts'

jest.mock('./parse-conversation', () => ({
  parseConversation: jest.fn(() => {
    const {
      sampleConversation,
      sampleConfig,
    } = require('../utils/test-utils.ts')
    return { conversation: sampleConversation, config: sampleConfig }
  }),
}))

jest.mock('./parse-conversation-configs', () => ({
  parseConversationConfigs: jest.fn(() => {
    const { sampleConfigsByName } = require('../utils/test-utils.ts')
    return sampleConfigsByName
  }),
}))

jest.mock('./dissect-html', () => ({
  dissectHtml: jest.fn(() => {
    const {
      sampleHtmlConversationText,
      sampleConfigName,
      sampleConfigsByNameJson,
    } = require('../utils/test-utils.ts')
    return {
      textSections: [''],
      conversationSections: [
        {
          text: sampleHtmlConversationText,
          configName: sampleConfigName,
        },
      ],
      configsText: sampleConfigsByNameJson,
    }
  }),
}))

describe(parseConversationCollection, () => {
  it('returns a conversation collection containing conversations and configs', () => {
    const collection = parseConversationCollection(
      sampleHtmlConversationCollectionText,
      'html',
    )

    expect(collection.textSections.length).toBe(1)
    expect(collection.conversations.length).toBe(1)
    expect(collection.configsByName).toEqual(sampleConfigsByName)
  })

  it('calls parseConversation on each conversation extracted from input', () => {
    parseConversationCollection(sampleHtmlConversationCollectionText, 'html', {
      [sampleConfigName]: sampleConfig,
    })
    expect(mockParseConversation).toHaveBeenCalled()
  })

  it('calls dissectHtml on HTML-formatted input', () => {
    parseConversationCollection(sampleHtmlConversationCollectionText, 'html', {
      [sampleConfigName]: sampleConfig,
    })
    expect(mockDissectHtml).toHaveBeenCalled()
  })
})
