import { type Conversation } from '../types/conversation.ts'
import { sampleConfig, sampleConfigName } from '../utils/test-utils.ts'
import { postprocessMarkdown } from './postprocess-markdown.ts'

describe(postprocessMarkdown, () => {
  const message = 'a *message* using [Markdown](#)'
  const expected = 'a <em>message</em> using <a href="#">Markdown</a>'

  const conversation: Conversation = {
    configName: sampleConfigName,
    messageCollections: [
      {
        username: sampleConfig.users[0].name,
        messages: [message],
      },
    ],
  }

  it('renders Markdown inside each message in a conversation and replaces the message in-place', () => {
    postprocessMarkdown(conversation)
    expect(conversation.messageCollections[0].messages[0]).toBe(expected)
  })
})
