import { marked } from 'marked'
import { type Conversation } from '../types/conversation.ts'

export function postprocessMarkdown(conversation: Conversation): void {
  for (const messageCollection of conversation.messageCollections) {
    messageCollection.messages = messageCollection.messages.map(
      (message) => marked.parseInline(message) as string,
    )
  }
}
