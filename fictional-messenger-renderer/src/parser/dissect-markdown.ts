import { marked, type TokensList } from 'marked'
import { type DissectedText } from './parse-conversation-collection.ts'

const conversationLangRegex = /^\s*fm:\s*(\S+)/
const configsLangRegex = /^\s*fm-configs/

function makeTokensList(): TokensList {
  const tokens: any = []
  tokens.links = {}
  return tokens as TokensList
}

export function dissectMarkdown(text: string): DissectedText {
  const textSectionTokens: TokensList[] = []
  const conversationSections: DissectedText['conversationSections'] = []

  const tokens = marked.lexer(text)

  let configsText: string | undefined
  let currentTextSection = makeTokensList()

  for (const token of tokens) {
    if (token.type === 'code') {
      const lang = token.lang as string | null
      const styleMatches = lang?.match(conversationLangRegex)
      const configsMatches = lang?.match(configsLangRegex)

      if (styleMatches != null) {
        const configName = styleMatches[1]
        conversationSections.push({ text: token.text, configName })

        textSectionTokens.push(currentTextSection)
        currentTextSection = makeTokensList()
        continue
      } else if (configsMatches != null) {
        configsText = token.text
        continue
      }
    }

    currentTextSection.push(token)
  }

  textSectionTokens.push(currentTextSection)

  const textSections: string[] = []
  for (const sectionTokens of textSectionTokens) {
    const textSection = marked.parser(sectionTokens)
    textSections.push(textSection)
  }

  return { textSections, conversationSections, configsText }
}
