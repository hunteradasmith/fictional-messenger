import {
  sampleConfigsByName,
  sampleConfigsByNameJson,
} from '../utils/test-utils.ts'
import { parseConversationConfigs } from './parse-conversation-configs.ts'

describe(parseConversationConfigs, () => {
  it('parses a JSON conversation config', () => {
    const parsed = parseConversationConfigs(sampleConfigsByNameJson, 'json')
    expect(parsed).toEqual(sampleConfigsByName)
  })
})
