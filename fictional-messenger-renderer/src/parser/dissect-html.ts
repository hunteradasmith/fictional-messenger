import parse, { type HTMLElement } from 'node-html-parser'
import { ConversationParseError } from '../errors/conversation-parse-error.ts'
import { type DissectedText } from './parse-conversation-collection.ts'

interface SourceRange {
  outerStart: number
  innerStart: number
  outerEnd: number
  innerEnd: number
  startRegexMatches: RegExpMatchArray
}
interface ConversationSourceRange extends SourceRange {
  configName: string
}

const elementsToCheckSelector = 'div, p, header, h1, h2, h3, h4, h5, h6'

/** Matches strings like "-- fm: configName --", with flexibility around whitespace and hyphen vs en dash vs em dash */
const conversationStartRegex = /^\s*[-–—]+\s*fm:\s*(\S+)(?:\s+[-–—]+)?/i

/** Matches strings like "-- fm-configs --", with flexibility around whitespace and hyphen vs en dash vs em dash */
const conversationConfigsStartRegex = /^\s*[-–—]+\s*fm-configs(?:\s+[-–—]+)?/i

/** Matches strings like "-- end --", with flexibility around whitespace and hyphen vs en dash vs em dash */
const conversationEndRegex = /^\s*[-–—]+\s*end(?:\s+[-–—]+)?/i

function findConversationSources(root: HTMLElement): ConversationSourceRange[] {
  return findSourceRanges(
    root,
    conversationStartRegex,
    conversationEndRegex,
  ).map((r) => ({
    ...r,
    configName: r.startRegexMatches[1],
  }))
}

function findConversationConfigs(root: HTMLElement): SourceRange | undefined {
  return findSourceRanges(
    root,
    conversationConfigsStartRegex,
    conversationEndRegex,
  )[0]
}

function findSourceRanges(
  root: HTMLElement,
  startRegex: RegExp,
  endRegex: RegExp,
): SourceRange[] {
  const sourceRanges: SourceRange[] = []
  let current: SourceRange | null = null

  for (const element of root.querySelectorAll(elementsToCheckSelector)) {
    if (current === null) {
      const matches = element.textContent.match(startRegex)

      if (matches !== null) {
        current = {
          outerStart: element.range[0],
          innerStart: element.range[1],
          outerEnd: -1,
          innerEnd: -1,
          startRegexMatches: matches,
        }
      }
    } else if (endRegex.test(element.textContent)) {
      current.innerEnd = element.range[0]
      current.outerEnd = element.range[1]
      sourceRanges.push(current)
      current = null
    }
  }

  if (current !== null) {
    throw new ConversationParseError(
      'A conversation start marker was found without a corresponding end marker',
    )
  }

  return sourceRanges
}

function getTextContent(textWithHtml: string): string {
  const root = parse(textWithHtml)
  return root.textContent
}

export function dissectHtml(text: string): DissectedText {
  const textSections: string[] = []
  const conversationSections: DissectedText['conversationSections'] = []

  let processedText: string = text
  let configsText: string | undefined

  let root = parse(processedText)
  const configsSource = findConversationConfigs(root)

  if (typeof configsSource !== 'undefined') {
    // getTextContent called here because the JSON in the config may have containing HTML elements that need removal
    configsText = getTextContent(
      processedText.slice(configsSource.innerStart, configsSource.innerEnd),
    )
    processedText =
      processedText.slice(0, configsSource.outerStart) +
      processedText.slice(configsSource.outerEnd)
  }

  // Re-parse root because processedText may have changed. Inefficient, TODO refactor
  root = parse(processedText)
  const conversationSources = findConversationSources(root)

  let lastSectionIndex = 0
  for (const {
    outerStart,
    innerStart,
    outerEnd,
    innerEnd,
    configName,
  } of conversationSources) {
    const textSection = processedText.slice(lastSectionIndex, outerStart)
    textSections.push(textSection)

    const conversationText = processedText.slice(innerStart, innerEnd)
    conversationSections.push({ text: conversationText, configName })

    lastSectionIndex = outerEnd
  }

  const finalTextSection = processedText.slice(lastSectionIndex)
  textSections.push(finalTextSection)

  return { textSections, conversationSections, configsText }
}
