import css, { type Stylesheet, type Declaration, type Rule } from 'css'
import {
  cssVarsToRule,
  getRuleBySelectors,
  parseStyleAttribute,
  prefixRuleSelectors,
  propertiesToRule,
  removeProperties,
  setPropertyValue,
  stringifyStyleAttribute,
} from './css-utils.ts'

const declarationByProperty = (rule: Rule, property: string): Declaration =>
  ((rule.declarations ?? []).find(
    (d) => (d as Declaration).property === property,
  ) as Declaration) ?? null

describe(getRuleBySelectors, () => {
  const stylesheet = css.parse(`
    .selector-1, .selector-2 {
      --prop: 'value';
    }

    .selector-3 {
      --prop: 'other-value';
    }
    `)

  const selectors = ['.selector-1', '.selector-2']

  it('selects rule by selectors', () => {
    const rule = getRuleBySelectors(stylesheet, selectors)
    expect(rule).toBe(stylesheet.stylesheet?.rules[0])
  })

  it('selects rule by selectors in different order', () => {
    const rule = getRuleBySelectors(stylesheet, selectors.reverse())
    expect(rule).toBe(stylesheet.stylesheet?.rules[0])
  })

  it('returns null on selectors not matching a rule', () => {
    const rule = getRuleBySelectors(stylesheet, ['.not-matching'])
    expect(rule).toBeNull()
  })
})

describe(setPropertyValue, () => {
  const propName = '--property-name'
  const originalValue = 'original-value'
  const newValue = 'new-value'

  const declaration: Declaration = {
    property: propName,
    value: originalValue,
    type: 'declaration',
  }

  const rule: Rule = {
    selectors: ['.selector-1'],
    declarations: [declaration],
    type: 'rule',
  }

  it('sets property value on existing declaration', () => {
    setPropertyValue(rule, propName, newValue)
    expect(declaration.value).toBe(newValue)
  })

  it('adds new declaration with property value if none exist', () => {
    const newPropName = '--new-property-name'
    setPropertyValue(rule, newPropName, newValue)

    const newDeclaration: Declaration = (rule.declarations ??
      [])[1] as Declaration
    expect(newDeclaration).not.toBeNull()
    expect(newDeclaration.type).toBe('declaration')
    expect(newDeclaration.property).toBe(newPropName)
    expect(newDeclaration.value).toBe(newValue)
  })
})

describe(removeProperties, () => {
  const propToRemove = 'prop-to-remove'
  const propToKeep = 'prop-to-keep'

  const stylesheet = css.parse(`
    * {
      ${propToRemove}: value;
      ${propToKeep}: value;
    }`)

  const rule = stylesheet.stylesheet?.rules[0] as Rule

  it('removes specified properties from a rule', () => {
    removeProperties(rule, [propToRemove])
    const remainingProperties = rule.declarations?.map(
      (d) => (d as Declaration).property,
    )
    expect(remainingProperties).not.toContain(propToRemove)
  })

  it('does not remove non-specified properties from a rule', () => {
    removeProperties(rule, [propToRemove])
    const remainingProperties = rule.declarations?.map(
      (d) => (d as Declaration).property,
    )
    expect(remainingProperties).toContain(propToKeep)
  })
})

describe(prefixRuleSelectors, () => {
  it("adds a prefix to rules' selectors", () => {
    const prefix = '#prefix'
    const rules: Rule[] = [
      { selectors: ['.selector-1', '.selector-2'], type: 'rule' },
      { selectors: ['.selector-3'], type: 'rule' },
    ]

    const prefixed = prefixRuleSelectors(rules, prefix)

    for (const rule of prefixed) {
      for (const selector of rule.selectors ?? []) {
        expect(selector.startsWith(prefix)).toBeTruthy()
      }
    }
  })
})

describe(propertiesToRule, () => {
  it('creates a new rule that sets css vars', () => {
    const selectors = ['.selector']
    const cssVars = { var1: 'value1', var2: 'value2' }
    const rule = propertiesToRule(cssVars, selectors)

    expect(rule.selectors).toEqual(selectors)

    for (const [property, value] of Object.entries(cssVars)) {
      const dec = declarationByProperty(rule, property)
      expect(dec).not.toBeNull()
      expect(dec.value).toBe(value)
    }
  })
})

describe(cssVarsToRule, () => {
  it('adds css var double-dash prefix when needed', () => {
    const prefixVar = (v: string): string => '--' + v
    const selectors = ['.selector']

    const unprefixed = 'unprefixed'
    const prefixed = '--prefixed'

    const cssVars = { unprefixed: 'value1', prefixed: 'value2' }
    const rule = cssVarsToRule(cssVars, selectors)

    const unprefixedDec = declarationByProperty(rule, prefixVar(unprefixed))
    expect(unprefixedDec).not.toBeNull()

    const prefixedDec = declarationByProperty(rule, prefixed)
    expect(prefixedDec).not.toBeNull()
  })
})

describe(parseStyleAttribute, () => {
  const styleText = 'a-property:a-value;another-property:another-value'

  it('parses css properties from a style string', () => {
    const parsed = parseStyleAttribute(styleText)
    expect(parsed).not.toBeNull()
    expect((parsed?.stylesheet?.rules[0] as Rule).declarations).toHaveLength(2)
  })
})

describe(stringifyStyleAttribute, () => {
  const styleText = 'a-property:a-value;another-property:another-value;'

  it('stringifies a stylesheet into a style string', () => {
    const parsed = parseStyleAttribute(styleText)
    const stringified = stringifyStyleAttribute(parsed as Stylesheet)
    expect(stringified).toBe(styleText)
  })
})
