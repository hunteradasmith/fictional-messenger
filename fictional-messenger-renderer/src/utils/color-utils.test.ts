import { HslColor } from '../types/color.ts'
import { classesForColor, nearestColor } from './color-utils.ts'

// Note: isValidColor is tested as a part of the Settings.isValueValid tests

describe(nearestColor, () => {
  it('returns the nearest valid color to the specified invalid color', () => {
    const invalidColor: HslColor = { hue: 1, sat: -100, lit: 1000 }
    const expectedColor: HslColor = { hue: 0, sat: 0, lit: 100 }
    expect(nearestColor(invalidColor)).toEqual(expectedColor)
  })

  it('returns an identical color for the specified valid color', () => {
    const validColor: HslColor = { hue: 25, sat: 20, lit: 50 }
    expect(nearestColor(validColor)).toEqual(validColor)
  })
})

describe(classesForColor, () => {
  it('returns the appropriate CSS classes for the specified color', () => {
    const fgColor: HslColor = { hue: 25, sat: 20, lit: 50 }
    const fgClasses = 'fg-hsl hue25 sat20 lit50'
    expect(classesForColor('fg', fgColor)).toEqual(fgClasses)

    const bgColor: HslColor = { hue: 25, sat: 20, lit: 50 }
    const bgClasses = 'bg-hsl hue25 sat20 lit50'
    expect(classesForColor('bg', bgColor)).toEqual(bgClasses)
  })
})
