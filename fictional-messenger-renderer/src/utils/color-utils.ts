/**
 * @privateRemarks Note: changes here must also be made to colors.scss in order
 * for color CSS classes to work properly.
 */

// TODO tests!

import lodash from 'lodash'
import { type HslColor } from '../types/color.ts'

const fgColorClass = 'fg-hsl'
const bgColorClass = 'bg-hsl'

interface ColorRange {
  start: number
  end: number
  step: number
}

export const colorRanges: Record<keyof HslColor, ColorRange> = {
  hue: { start: 0, end: 355, step: 5 },
  sat: { start: 0, end: 100, step: 10 },
  lit: { start: 0, end: 100, step: 10 },
}

const colorKeys: (keyof HslColor)[] = ['hue', 'sat', 'lit']

function isInRange(value: number, { start, end, step }: ColorRange): boolean {
  return lodash.range(start, end + step, step).includes(value)
}

function roundToRange(value: number, { start, end, step }: ColorRange): number {
  const mod = value % step
  const rounded = mod >= step / 2 ? value + (step - mod) : value - mod
  return rounded < start ? start : rounded > end ? end : rounded
}

/**
 * Returns a boolean indicating whether the provided {@link HslColor} is valid.
 */
export function isValidColor(color: HslColor): boolean {
  if (color === null || typeof color !== 'object') {
    return false
  }

  return (
    colorKeys.every((k) => Object.hasOwn(color, k)) &&
    colorKeys.every((k) => isInRange(color[k], colorRanges[k]))
  )
}

/**
 * Returns {@link HslColor} that is the nearest valid color to the
 * provided, possibly-invalid {@link HslColor}.
 */
export function nearestColor(color: HslColor): HslColor {
  const nearest: HslColor = { ...color }

  for (const key of colorKeys) {
    nearest[key] = roundToRange(color[key], colorRanges[key])
  }

  return nearest
}

/**
 * Returns a string containing CSS classes for the provided {@link HslColor}.
 */
export function classesForColor(target: 'fg' | 'bg', color: HslColor): string {
  const targetClass = target === 'fg' ? fgColorClass : bgColorClass
  const colorClasses = colorKeys.map((k) => `${k}${color[k]}`)
  return [targetClass, ...colorClasses].join(' ')
}
