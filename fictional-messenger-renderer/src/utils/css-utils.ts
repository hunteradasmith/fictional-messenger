import css, {
  type Declaration,
  type Rule,
  type Stylesheet,
  type Comment,
} from 'css'
import lodash from 'lodash'

function isType<T extends { type: string }>(
  value: { type: string },
  type: T['type'],
): value is T {
  return value.type === type
}

/**
 * Finds the first CSS rule that exactly matches the specified selectors, or null if the selector could not be found.
 *
 * @param stylesheet - {@link Stylesheet} from which the {@link Rule} will be found
 * @param selectors - CSS selectors that must exactly match the target {@link Rule}
 * @returns - Matching {@link Rule}, or null if no matching rule was found
 */
export function getRuleBySelectors(
  stylesheet: Stylesheet,
  selectors: string[],
): Rule | null {
  const rule: Rule | null =
    stylesheet.stylesheet?.rules
      .filter((obj) => isType<Rule>(obj, 'rule'))
      .find((obj) =>
        lodash.isEqual(
          lodash.sortBy((obj as Rule).selectors),
          lodash.sortBy(selectors),
        ),
      ) ?? null

  return rule
}

/**
 * Sets a CSS property on a {@link Rule} to a particular value, replacing any existing declarations of the property.
 *
 * @param rule - {@link Rule} to be modified
 * @param property - property to be set
 * @param value - value to be set
 */
export function setPropertyValue(
  rule: Rule,
  property: string,
  value: string,
): void {
  const dec: Declaration | null =
    rule.declarations
      ?.filter((obj) => isType<Declaration>(obj, 'declaration'))
      .find((obj) => obj.property === property) ?? null

  if (dec != null) {
    dec.value = value
  } else {
    const newDec: Declaration = { property, value, type: 'declaration' }
    rule.declarations ||= []
    rule.declarations.push(newDec)
  }
}

/**
 * Removes specified CSS properties from a {@link Rule}, if present.
 *
 * @param rule - {@link Rule} to be modified
 * @param properties - properties to to removed
 */
export function removeProperties(rule: Rule, properties: string[]): void {
  if (typeof rule.declarations === 'undefined') {
    return
  }

  const newDeclarations: (Comment | Declaration)[] = []

  for (const decOrComment of rule.declarations) {
    if (isType<Declaration>(decOrComment, 'declaration')) {
      if (
        typeof decOrComment.property !== 'undefined' &&
        properties.includes(decOrComment.property)
      ) {
        continue
      }
    }

    newDeclarations.push(decOrComment)
  }

  rule.declarations = newDeclarations
}

/**
 * For each provided {@link Rule}, adds a prefix to all of the rule's selectors.
 *
 * @param rules - {@link Rule | Rules} whose selectors will be prefixed
 * @param prefix - prefix to be applied to each {@link Rule}
 * @returns {@link Rule | Rules} with prefixed selectors
 */
export function prefixRuleSelectors(rules: Rule[], prefix: string): Rule[] {
  const newRules = lodash.cloneDeep(rules)

  for (const rule of newRules) {
    rule.selectors = rule.selectors?.map((sel) => `${prefix} ${sel}`)
  }

  return newRules
}

/**
 * Creates a new {@link Rule} from specified properties and values.
 *
 * @param properties - mapping of properties to values
 * @param selectors - CSS selectors to be assigned to the new {@link Rule}
 * @returns a new {@link Rule} that sets the specified CSS properties
 */
export function propertiesToRule(
  properties: Record<string, string>,
  selectors: string[],
): Rule {
  return {
    selectors,
    type: 'rule',
    declarations: Object.entries(properties).map(([property, value]) => ({
      property,
      value,
      type: 'declaration',
    })),
  }
}

/**
 * Creates a new {@link Rule} from specified CSS variables and values.
 *
 * @remarks
 * The property names of variables will have the "--" prefix added if not already present.
 *
 * @param vars - mapping of CSS variable properties to values
 * @param selectors - CSS selectors to be assigned to the new {@link Rule}
 * @returns a new {@link Rule} that sets the specified CSS variables
 */
export function cssVarsToRule(
  vars: Record<string, string>,
  selectors: string[],
): Rule {
  const prefixedVars = Object.fromEntries(
    Object.entries(vars).map(([k, v]) => [
      k.startsWith('--') ? k : '--' + k,
      v,
    ]),
  )

  return propertiesToRule(prefixedVars, selectors)
}

/**
 * Parses CSS rules from an HTML style attribute value into a {@link Stylesheet}.
 *
 * Empty strings, or strings with invalid CSS, will cause a null value to be returned.
 *
 * @param styleText - style value as text
 * @returns a {@link Stylesheet} parsed from the style attribute value
 */
export function parseStyleAttribute(styleText: string): Stylesheet | null {
  if (styleText.length === 0) return null

  const wrapped = `*{${styleText}}`

  try {
    const parsed = css.parse(wrapped)
    return parsed ?? null
  } catch {
    return null
  }
}

/**
 * Stringifies a {@link Stylesheet} into an HTML style attribute value.
 *
 * @param stylesheet - {@link Stylesheet} to stringify
 * @returns a style attribute value stringified from the stylesheet
 */
export function stringifyStyleAttribute(stylesheet: Stylesheet): string {
  const style = css.stringify(stylesheet, { compress: true })
  const unwrapped = style.slice(2, -1)
  return unwrapped
}

/**
 * Joins CSS class names together with spaces.
 */
export function joinClassNames(...classNames: (string | undefined)[]): string {
  return classNames.filter((n) => n !== undefined && n !== null).join(' ')
}
