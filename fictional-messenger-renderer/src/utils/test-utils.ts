import { type ConversationCollection } from '../types/conversation-collection.ts'
import { type ConversationConfig } from '../types/conversation-config.ts'
import { type Conversation } from '../types/conversation.ts'
import { type MessageCollection } from '../types/message-collection.ts'

export const sampleConfig: ConversationConfig = {
  style: 'basic',
  settingValues: {},
  users: [
    {
      name: 'user1',
      displayName: 'User 1',
      settingValues: {},
    },
    {
      name: 'user2',
      displayName: 'User 2',
      settingValues: {},
    },
  ],
}

export const sampleConfigName = 'sample-config'

export const sampleConfigsByName: Record<string, ConversationConfig> = {
  [sampleConfigName]: sampleConfig,
}

export const sampleConfigsByNameJson: string =
  JSON.stringify(sampleConfigsByName)

export const sampleConversationText = `
user1: Test message one
user2: Test message two
user2: Test message three
`

export const sampleConversationMessageCollections: MessageCollection[] = [
  {
    username: sampleConfig.users[0].name,
    messages: ['Test message one'],
  },
  {
    username: sampleConfig.users[1].name,
    messages: ['Test message two', 'Test message three'],
  },
]

export const sampleConversation: Conversation = {
  messageCollections: sampleConversationMessageCollections,
  configName: sampleConfigName,
}

export const sampleConversationCollection: ConversationCollection = {
  configsByName: sampleConfigsByName,
  textSections: ['text section one', 'text section two'],
  conversations: [sampleConversation],
}

export const sampleHtmlConversationText = `
<p><span>user1: Test message one</span></p>
<p>user2: <em>Test message two</em></p>
<p><span>user2:</span> <em>Test message three</em></p>
`

export const sampleHtmlConversationCollectionSections = [
  '<p>-- fm-configs --</p>',
  sampleConfigsByNameJson,
  '<p>-- end --</p>',
  '<p>First non-conversation HTML section</p>',
  `<p>-- fm: ${sampleConfigName} --</p>
  ${sampleHtmlConversationText}
  <p>-- end --</p>`,
  '<p>Second non-conversation HTML section</p>',
  `<p>-- fm: ${sampleConfigName} --</p>
  ${sampleHtmlConversationText}
  <p>-- end --</p>`,
  '<p>Third non-conversation HTML section</p>',
]

export const sampleHtmlConversationCollectionText =
  sampleHtmlConversationCollectionSections.join('\n')

export const sampleMarkdownConversationText = `
user1: Test message one
user2: Test message two
user2: *Test message three*
`
export const sampleMarkdownConversationCollectionSections = [
  '```fm-configs',
  sampleConfigsByNameJson,
  '```',
  'First non-conversation Markdown section',
  `\`\`\`fm: ${sampleConfigName}`,
  sampleMarkdownConversationText,
  '```',
  'Second non-conversation Markdown section',
  `\`\`\`fm: ${sampleConfigName}`,
  sampleMarkdownConversationText,
  '```',
  'Third non-conversation Markdown section',
]

export const sampleMarkdownConversationCollectionText =
  sampleMarkdownConversationCollectionSections.join('\n')

// Sample conversation in various real-world HTML formats
export const otherSampleHtmlSections = {
  // Direct entry into contentEditable
  'direct entry': `<div>-- fm:sample-config --</div><div>user1: Test message one</div><div>user2: Test message two</div><div>user2: Test message two</div><div>-- end --</div>`,

  // Google Docs generated HTML
  'google docs': `<p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;" id="docs-internal-guid-b2c53784-7fff-819d-25b0-b29f7ebde3c1"><span style="font-size:11pt;font-family:Arial,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">– fm: sample-config –</span></p><p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">user1: Test message one</span></p><p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">user2: Test message two</span></p><p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">user2: Test message three</span></p><p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:11pt;font-family:Arial,sans-serif;color:#000000;background-color:transparent;font-weight:400;font-style:normal;font-variant:normal;text-decoration:none;vertical-align:baseline;white-space:pre;white-space:pre-wrap;">– end –</span></p>`,

  // LibreOffice Writer generated HTML
  'libreoffice writer': `<p style="line-height: 100%; margin-bottom: 0in">
-- fm: sample-config --</p>
<p style="line-height: 100%; margin-bottom: 0in">user1: Test message
one</p>
<p style="line-height: 100%; margin-bottom: 0in">user2: Test message
two</p>
<p style="line-height: 100%; margin-bottom: 0in">user2: Test message
three</p>
<p style="line-height: 100%; margin-bottom: 0in">-- end --</p>
<p style="line-height: 100%; margin-bottom: 0in"><br>

</p>

<style type="text/css">p { line-height: 115%; margin-bottom: 0.1in; background: transparent }</style>`,
}
