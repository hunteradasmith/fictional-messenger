import { sampleConversationCollection } from '../utils/test-utils.ts'
import { renderConversationCollection } from './render-conversation-collection.ts'

describe(renderConversationCollection, () => {
  it('returns rendered html and css', () => {
    const result = renderConversationCollection(sampleConversationCollection)

    expect(result.html).toBeTruthy()
  })
})
