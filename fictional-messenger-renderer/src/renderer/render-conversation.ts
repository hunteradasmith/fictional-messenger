import { ConversationConfigError } from '../errors/conversation-config-error.ts'
import { type Conversation } from '../types/conversation.ts'
import { styles } from '../styles/index.ts'
import { type RenderResult } from '../types/render-result.ts'
import { ConversationConfig } from '../types/conversation-config.ts'

/**
 * Renders conversation data into HTML and CSS
 *
 * @param conversation - {@link Conversation} data to be rendered
 * @returns the rendered HTML and CSS
 *
 * @throws {@link ConversationParseError}
 * Thrown if the provided conversation text is not formatted correctly
 *
 * @throws {@link TypeError}
 * Thrown if a style or user setting fails validation
 *
 * @throws {@link ConversationConfigError}
 * Thrown for errors in the conversation configuration
 */
export function renderConversation(
  conversation: Conversation,
  config: ConversationConfig,
): RenderResult {
  const constructor = styles.constructorByName[config.style]

  if (constructor === undefined) {
    throw new ConversationConfigError(
      `Unable to find style by name "${config.style}"`,
    )
  }

  const style = new constructor(conversation, config)
  const html = style.renderHtml()

  return { html }
}
