import { ConversationConfigError } from '../errors/conversation-config-error.ts'
import { renderConversation } from './render-conversation.ts'
import { sampleConfig, sampleConversation } from '../utils/test-utils.ts'
import { ConversationConfig } from '../types/conversation-config.ts'

describe(renderConversation, () => {
  it('returns rendered HTML for valid conversations & configs', () => {
    const result = renderConversation(sampleConversation, sampleConfig)

    expect(result.html.length).toBeGreaterThan(0)
  })

  it('throws an error on configs with non-existing style names', () => {
    const invalidConfig: ConversationConfig = {
      style: 'invalid-style',
      settingValues: {},
      users: [],
    }

    expect(() => renderConversation(sampleConversation, invalidConfig)).toThrow(
      ConversationConfigError,
    )
  })
})
