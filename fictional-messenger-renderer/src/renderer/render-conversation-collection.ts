import lodash from 'lodash'
import { type ConversationCollection } from '../types/conversation-collection.ts'
import { renderConversation } from './render-conversation.ts'
import { type RenderResult } from '../types/render-result.ts'
import { ConversationConfigError } from '../errors/conversation-config-error.ts'

/**
 * Renders conversation collection data into HTML and CSS
 *
 * @param conversationCollection - {@link ConversationCollection} data to be rendered
 * @returns the rendered HTML and CSS
 *
 * @throws {@link ConversationParseError}
 * Thrown if a provided conversation's text is not formatted correctly
 *
 * @throws {@link TypeError}
 * Thrown if a style or user setting fails validation
 *
 * @throws {@link ConversationConfigError}
 * Thrown for errors in a conversation configuration
 */
export function renderConversationCollection(
  conversationCollection: ConversationCollection,
): RenderResult {
  const renderResults = conversationCollection.conversations.map((c) => {
    if (typeof c.configName === 'undefined') {
      throw new ConversationConfigError(
        'All conversations in a collection must have defined config names',
      )
    }

    const config = conversationCollection.configsByName[c.configName]

    if (typeof config === 'undefined' || config === null) {
      throw new ConversationConfigError(
        `Conversation collection is missing a config by name "${c.configName}`,
      )
    }
    return renderConversation(c, config)
  })

  const renderedSections = renderResults.map(({ html }) => html)

  const zippedSections = lodash
    .zip(conversationCollection.textSections, renderedSections)
    .flat()
  const html = zippedSections.join('')

  return { html }
}
