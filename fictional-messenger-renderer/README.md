# fictional-messenger-renderer

fictional-messenger-renderer is a Node.js module that renders text conversations with fancy HTML and CSS to imitate real-world message platforms. This module is used by the [Fictional Messenger web app](https://fictional.tools/messenger/?ref=gl).

## Development info

#### Setting up the project
Run `npm install` to install the needed dependencies.

## Building, Linting, and Testing
```bash
npm run test # lints, prettifies, checks TypeScript, and runs Jest unit tests
npm run test:lint:fix # lints in auto-fix mode
npm run build # builds the module to dist/
```

## Style CSS building

CSS for each conversation style is written as SCSS in `scss/styles/`.

The `scripts/pack-css.mjs` script is used to build the SCSS to CSS, then store it as a string in the auto-generated file `src/generated/conversation-css.ts`. This allows the CSS to be used within the module.
