// @ts-check

import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';
import eslintConfigPrettier from 'eslint-config-prettier';
import eslintPluginPrettierRecommended from 'eslint-plugin-prettier/recommended';
import eslintPluginImportX from 'eslint-plugin-import-x'

export default tseslint.config(
  eslint.configs.recommended,
  eslintPluginImportX.flatConfigs.recommended,
  tseslint.configs.recommended,
  tseslint.configs.stylistic,
  eslintConfigPrettier,
  eslintPluginPrettierRecommended,
  {
    extends: [],
    rules: {
      '@typescript-eslint/no-explicit-any': 'off',
      'import-x/extensions': ['error', 'ignorePackages'],
    },
    settings: {
      'import-x/resolver': {
        'typescript': {
          project: './tsconfig.json',
        },
      },
    },
  }
);