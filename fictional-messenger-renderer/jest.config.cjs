module.exports = {
  roots: ['<rootDir>/src'],
  transform: {
    '^.+\\.(t|j)sx?$': ['babel-jest', { configFile: './babel.jest.config.json' }],
  },
  moduleNameMapper: {
    "(.+)\\.js": "$1",
    "(.+)\\.ts": "$1"
  },
  extensionsToTreatAsEsm: [".ts", ".tsx"],
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
}
